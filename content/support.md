+++
date = "2021-10-22"
draft = false
title = "Unterstützen"
metadescription = "Josa Wode möchte Schreiben. Ihr könnt ihm dabei helfen, z.B. mit Geld, Feedback oder vielleicht sogar Zusammenarbeit (illustrieren / schreiben / ?)."
+++

### It´s all about the money   

*Was mir hilft weiter zu machen ist natürlich positives Feedback, Luft und Liebe.*

Schreib mir gerne, wenn du mir etwas zu einer meiner Geschichten mitteilen möchtest. Ich freue mich wirklich darüber. Dann merke ich, dass ich das Ganze nicht nur für mich mache und abgeschottet in meiner eigenen Blase lebe. 

Auch Korrekturvorschläge oder sonstige Hinweise, wie ich Dinge besser machen kann, sind wertvoll für mich.

Für all dies kannst du mich gerne <a href="&#x6d;&#97;&#105;&#x6c;&#116;&#x6f;&#58;&#119;&#x72;&#x69;&#x74;&#105;&#110;&#103;&#64;&#102;&#x6f;&#x74;&#x6f;&#x65;&#108;&#101;&#99;&#x74;&#x72;&#105;&#99;&#115;&#x2e;&#x64;&#101;">kontaktieren</a>.

*Ich würde aber auch Geld nehmen.*

Ich freue mich genauso über jeden Euro, den ich für eine Geschichte bekomme.
Ich mag es, wenn Dinge frei zugänglich sind und Leute daran teilhaben können, die sich das sonst nicht leisten könnten.  
Falls du mir aber etwas Geld für eine meiner Geschichten geben kannst und magst, ist das finde ich das eine schöne Wertschätzung. 

Geld kannst du mir über Paypal zukommen lassen -- unter jedem Beitrag findest du einen entsprechenden Link.

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="7S8QXB9BNSUUS">
<input type="image" src="https://josawode.de/img/paypal-button-big.png" border="0" name="submit" alt="Jetzt einfach, schnell und sicher online bezahlen – mit PayPal.">
<img alt="" border="0" src="https://www.paypalobjects.com/de_DE/i/scr/pixel.gif" width="1" height="1">
</form>

<br/>
<b>Ich freue mich über jede Unterstützung</b>
