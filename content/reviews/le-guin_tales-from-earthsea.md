+++
author = "Ursula K. Le Guin"
date = "2017-12-31"
description = "Abtauchen in eine Welt voller Sagen"
edition = "2012"
hardcover = "no"
img = "/img/reviews/le-guin_tales-from-earthsea.jpg"
isbn = "978-0-547-77370-4"
language = "en"
pages = "456"
publisher = "Houghton Mifflin Harcourt"
subtitle = ""
title = "Tales from Earthsea"

+++
Bereits vor Beginn der Lektüre von *Tales from Eartsea* freute ich mich auf meine Rückkehr in diese fantastische Welt. Erst vor wenigen Jahren tauchte ich in einem Urlaub erstmals in diese -- bereits im Jahr 1968 unserer Zeit für Leser zugängliche -- Welt ein. Wenige Tage (und Nächte) später hatte ich die vier bis dahin erschienenen Bände *A Wizard of Earthsea*, *The Tombs of Atuan*, *The Farthest Shore*, und *Tehanu* (im Sammelband *The Earthsea Quartet*, ISBN: 978-0-14-034803-3) verschlungen.

Der unvergleichliche Schreibstil Le Guins brachte mir diese Welt und ihre Bewohner so nahe, dass sie für mich fast real wurden. Ich saß am Kamin und lauschte den Sagen von Zauberern, Priesterinnen, Drachen und dem Wesen der Dinge. In *A Wizard of Earthsea* wurde ich dabei immer wieder auf unangenehme Geschlechterbilder gestoßen ("Weak as woman's magic [...] Wicked as woman's magic"). Die Geschichte erzählt von der persönlichen Entwicklung und den Abenteuern eines Zauberers. *The Tombs of Atuan* zeigte mir die Welt einer jungen Hohepriesterin und wie wir in unseren gesellschaftlichen Normen und Regeln gefangen sind. *The Farthest Shore* vereint die Themen persönliche Entwicklung, Gleichgewicht der Dinge und Opferbereitschaft. *Tehanu* kehrt (fast 20 Erdenjahre später) mit Wehemenz zur Geschlechterungerechtigkeit der Welt zurück und hinterfragt diese, beleuchtet dabei Earthsea von einer neuen Seite. Persönliche Entwicklung ist wieder ein zentrales Thema, wobei diesmal die emotionale Ebene im Fokus steht.



Auch in den Geschichten in *Tales from Earthsea* steckt mehr als das träumerische Abtauchen in eine fremde Welt. Sie sprechen wichtige gesellschaftliche Themen an und zeigen Menschen so wie sie sind. Le Guin kritisiert Missstände, ohne dabei den Zeigefinger zu erheben. Sie bleibt bei einer nachvollziehbaren Schilderung ihrer Welt und der dortigen Ereignisse. Ihr unvergleichlicher Schreibstil transportiert die Stimmung und was in den Charakteren vorgeht mit viel Geschick und Einfühlungsvermögen.



*The Finder* folgt der Entwicklung eines magisch begabten Mannes von der Jugend bis ins Alter. Er stellt sich gegen Unterdrückung und droht mehrfach daran zu zerbrechen. Immer wieder sind es -- vor Allem, aber nicht nur -- starke Frauen, die ihn davor bewahren und ihm ermöglichen seinen Kampf fortzusetzen. Es geht um gemeinsame Stärke sowie das Entstehen einer Gemeinschaft.



*Darkrose and Diamond* thematisiert Liebe, Freundschaft und Selbstfindung. Ein junger Mann sieht sich gezwungen, schwere Zukunftsentscheidungen zu treffen und sich in seinem Werdegang festzulegen.



*The Bones of the Earth* handelt von der Beziehung zwischen einem Zauberer und seinem Lehrling, von Demut, Macht und Verantwortung.



*On the High March* greift die Themen Ausnutzung und Hilfsbereitschaft auf. Im Kern der Geschichte geht es jedoch um Macht und die Versuchung, die von ihr ausgeht.



In *Dragonfly* beschreitet eine Frau, in der große -- nicht nur magische -- Kraft ruht, ihren Weg. Sie möchte in die, nur für männliche Zauberer offene, Schule der Zauberei aufgenommen werden, um zu lernen.



Ich kann die Lektüre der *Earthsea*-Reihe allen ans Herz legen -- am besten in der hier vorgestellten Reihenfolge. Ich selbst freue mich schon, dass es für mich bald mit *The Other Wind* weiter geht. Dieser derzeit letzte *Earthsea*-Roman liegt bereits auf meinem Nachttisch bereit.
