+++
author = "Neil Gaiman"
date = "2017-04-24"
description = "Entführung ins Reich der Träume"
edition = "2006* (s.u.)"
hardcover = "yes"
img = ""
isbn = "9781401210823*"
language = "en"
pages = "612*"
publisher = "Vertigo (DC)"
subtitle = ""
title = "The Absolute Sandman und Sandman Overture"

+++
Ich lese nur selten Comics oder Graphic Novels. Zu dieser Reihe hat mich meine Begeisterung für Neil Gaiman gebracht. Als ich den Sammelband The Absolute Sandman Vol. I in den Händen hielt, hat es mich sogleich in seinen Bann gezogen. Diese Luxusausgabe hat schon mit der Gestaltung des Schubers und des Einbandes alles richtig gemacht und das Umblättern der ersten Seiten hatte bereits etwas magisches. 

Doch auch die Handlung und Neil Gaimans Schreibstil haben mich sehr schnell gefesselt, sodass ich den ersten Band in kürzester Zeit verschlungen hatte und mich beherrschen musste, nicht gleich den zweiten Band zu bestellen. Denn der Preis auf die Seiten oder Einzelcomics gerechnet, ist zwar eher gering, doch haben (je ca.) 600 A4-Seiten in Farbe eben ihren Preis. Es gelang mir, mich zwischen den Bänden zu bremsen und so konnte ich jeden einzelnen aufs Neue als etwas ganz besonderes zelebrieren.

In den ersten Comics wird Dream, der Herr der Träume, (der Sandman hat viele Namen und Gesichter) im London des beginnenden 20. Jahrhunderts von einer Gruppe Magier gefangengesetzt und dies hat dramatische Folgen. Nach diesem Auftakt der Reihe, geht es sehr abwechslungsreich weiter -- teils werden kleinere Geschichten in einem Comic abgehandelt und teils erstreckt sich die Handlung über mehrere Einzelausgaben. Das Thema Träume und der nicht an Zeit gebundene Protagonist, der oft auch eher im Hintergrund steht, bieten vielfältige Möglichkeiten, die Gaiman auch nutzt. Es werden verschiedenste Zeitalter, Genres, gar andere Welten abgedeckt. Oft tun sich dabei (menschliche) Abgründe auf und der Horror kommt nicht zu kurz, doch kann es auch romantisch, traurig oder witzig werden. Neben Morpheus lernt man auch die anderen der Endless, seine ebenso faszinierenden Geschwister, kennen. Die Zusammenhänge und Verstrickungen dieser Familie könnte man wohl als grobe Rahmenhandlung auffassen. Daneben gibt es noch einige weitere Charaktere, die wiederholt auftreten.

Im Laufe der Reihe wechseln immer mal wieder Künstler und Zeichenstile (nicht immer fällt beides zusammen), doch wird stets höchste Qualität geliefert, sodass jede Seite ein Kunstwerk für sich darstellt. Dies unterstreicht und unterstützt noch die Vielseitigkeit der Handlung.
Viel Bonusmaterial bietet Einblicke hinter die Kulissen und rundet jeden Band ab.

Jahre nach dem Ende der Serie, kehrt Neil Gaiman mit The Sandman Overture zu dieser zurück, um einige lose Enden aufzugreifen und eine Geschichte zu erzählen, die in der Reihe bis dahin keinen Platz gefunden hatte. Nicht nur die Handlung, mit der drohenden Zerstörung des Universums, ist dabei sehr episch angelegt, auch in der Gestaltung wird nochmal das extrem hohe Niveau ein Stück nach oben gesetzt. Durch die Erzählung gewinnt der Charakter Dream weiter an Tiefe, aber auch andere der Endless werden nochmal von einer anderen Seite beleuchtet. 

Ich kann die ganze Reihe uneingeschränkt empfehlen und rate auch zur Anschaffung dieser liebevoll gemachten Luxusausgaben. Zumindest für Leute, die weiterlesen wollen, wird sich das preislich auch nicht sehr von den Einzelausgaben oder kleineren Sammelbänden mit je 5 Ausgaben in einem Taschenbuch unterscheiden und die Aufmachung ist, wie schon gesagt, sehr schön.

Im Kasten oben finden sich die Informationen zu **The Absolute Sandman, Vol. I**. Hier die Informationen zu den anderen Bänden:

The Absolute Sandman, Vol. II  
Vertigo (DC), 2007  
ISBN: 9781401210830  
Gebunden, 616 Seiten  

The Absolute Sandman, Vol. III  
Vertigo (DC), 2008  
ISBN: 9781401210847  
Gebunden, 616 Seiten  

The Absolute Sandman, Vol. IV  
Vertigo (DC), 2008  
ISBN: 9781401210854  
Gebunden, 608 Seiten  

The Absolute Sandman, Vol. V  
Vertigo (DC), 2011  
ISBN: 9781401232023  
Gebunden, 520 Seiten  

The Sandman Overture Deluxe Edition  
Vertigo (DC), 2015  
ISBN: 9781401248963  
Gebunden, 224 Seiten  
