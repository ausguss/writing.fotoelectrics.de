+++
author = "Claus Mattheck"
date = "2017-03-13"
description = "Anschauliche Darstellung des Verhaltens von Bäumen"
edition = "4. überarbeitete Auflage 2010"
hardcover = "no"
isbn = "9783923704729"
language = "de"
pages = "120"
publisher = "Karlsruher Institut für Technologie"
title = "Stupsi erklärt den Baum"
subtitle = "Ein Igel erklärt die Körpersprache der Bäume"
img = "/img/reviews/mattheck_stupsi.jpg"
+++

Mit meist sehr anschaulichen Zeichnungen und Photographien, ergänzt durch kurze Texte in einfacher Sprache, werden verschiedenste Phänomene und Eigenschaften von Bäumen dargestellt. Der Igel Stupsi begleitet den Leser durch das Buch, hilft häufig der Veranschaulichung, wirkt aber gelegentlich auch etwas in die Darstellung hineingezwängt.

Stellenweise erscheint mir das Buch etwas platt bzw. pädagogisch, didaktisch oder literarisch schwach. Mitunter glückt der Versuch des Spagats zwischen fachlicher Darstellung und Kinderbuch nicht ganz. 

Insgesamt halte ich das Buch jedoch für sehr gelungen -- ein toller Ansatz, verschiedene Phänomene am Baum auf einfache und anschauliche Weise darzustellen.

