+++
author = "Lewis Carroll"
date = "2017-04-28"
description = "Ein Klassiker mit weitreichendem Einfluss"
edition = "1976 (unverändert seit überarbeiteter Ausgabe 1970)"
hardcover = "no"
img = ""
isbn = "9780140013870"
language = "en"
pages = "352"
publisher = "Penguin Books"
subtitle = ""
title = "The Annotated Alice"

+++
Dies ist eines der Werke, die einen enormen Einfluss auf Kultur und Gesellschaft hatten und zum Teil immer noch haben. Das gestaltet eine Rezension besonders schwierig. Auch wenn ich die Geschichte von Alice bisher nie im Original gelesen hatte, ist mir doch fast alles was sich darin findet im Vorfeld schon in irgendeiner Weise bekannt gewesen. Denn fast alles aus dieser Geschichte wurde zahlreich aufgegriffen, abgewandelt, neu interpretiert und weitergesponnen. So mögen die originellsten Ideen aus heutiger Sicht abgedroschen wirken. Dazu kamen die großen Erwartungen an dieses beliebte Werk, die ich nicht ganz vermeiden konnte.

So ergab es sich wohl, dass mich der erste Teil -- Alice's Adventures in Wonderland -- eher langweilte und mir weitgehend wie eine bedeutungslose Aneinanderreihung schlechter Witze erschien. Im zweiten Teil -- Through the Looking-Glass and What Alice Found There -- wandelte sich dies jedoch. Auch hier ließe sich eine simple Aneinanderreihung von Einzelereignissen unterstellen, doch sind diese besser in einen Rahmen eingebettet und konnten mich -- jedes für sich -- mehr fesseln und mein Interesse wecken.

Im ersten Teil fällt Alice einer Tagträumerei anheim, die sie -- auf den Spuren des weißen Hasen -- in eine andere Welt führt. Dort liefert sie sich Streitgespräche mit diversen kuriosen Bewohnern und muss manche Gefahr überstehen, doch scheint bei ihr stets die Neugier über die Angst zu siegen und sie erfreut sich an dieser Welt, die für mich beim Lesen eher bedrohlich und abstoßend denn heiter wirkte.

Im zweiten Teil ergeht es ihr ganz ähnlich, nur dass sie sich diesmal durch einen Spiegel in das Wunderland begiebt. Carroll greift immer wieder das Motiv der gespiegelten Welt auf, sodass vieles dort, einfach gesagt, "andersherum" als in unserer Wirklichkeit funktioniert (vom Ziel weglaufen, um es zu erreichen). Das zweite große Motiv der Erzählung ist eine Schachpartie, in die die Handlung eingebettet ist und bei der Alice als Bauer das Brett überquert und, neben anderen Begegnungen, mit weiteren Figuren dieses Schachspiels zu tun bekommt.

Die Illustrationen der Originalausgaben von John Tenniel fügen sich gut in die Erzählung ein.

Martin Gardners Kommentare helfen, das Werk im Kontext seiner Zeit und zu verstehen, und zeigen auf, dass Carroll die Geschichte für die jungen Liddell-Schwestern -- insbesondere Alice --, erfunden hat. So werden auch zahlreiche Anspielungen, die sich an die Mädchen richten, erklärt. Recht ausführlich wird auch Carrolls extreme Fixierung auf junge Mädchen besprochen. Darüber hinaus wird bei den Gedichten und Liedern fast immer der Zusammenhang zu einem parodierten Original aufgezeigt. Auch Zusammenhänge zu Logik, Mathematik und weiterem werden hergestellt. Daher kann ich die Kommentare, auch wenn sie mich oft eher störend aus der Geschichte gerissen haben, sehr empfehlen.

Alice ist eine weibliche Heldin, was leider viel zu selten in der Literatur vorkommt, doch ist ihr Auftreten auch von vielen Klischees durchzogen, diese habe ich aber in der Erzählung nicht als sehr störend empfunden.

Insgesamt kann ich die Lektüre von Alice, insbesondere der kommentierten Ausgabe, sehr empfehlen, da einem dieses Werk oder (veränderte) Teile daraus sehr häufig begegnen und somit vieles in einem anderen (etwas helleren) Licht gesehen werden kann. Und auch die Geschichte für sich ist durchaus einen Blick wert, nur wird einem aus heutiger Sicht wohl nicht viel neues dabei begegnen.
