+++
author = "Carol Emshwiller"
date = "2017-02-26"
description = "Bewegend und einfühlsam"
edition = "2005"
hardcover = "no"
img = ""
isbn = "0142403024"
language = "en"
pages = "232"
publisher = "Firebird (Penguin Group)"
subtitle = ""
title = "The Mount"

+++
Die Idee hinter The Mount schien mir zunächst etwas albern: Aliens unterwerfen die Menschen und machen sie zu ihren Reittieren. Doch es wird schnell deutlich, dass dieses Thema viel Potential hat und der Autorin gelingt es, dieses mit sehr viel Einfühlungsvermögen auszuschöpfen. Dabei erlebt der*die Leser*in die Welt aus den Augen eines Heranwachsenden, der von klein auf darauf dressiert wurde, das Reittier für einen Prinzen zu sein. Zwischen den beiden entstand und entsteht zudem eine sehr enge Bindung. 
Es entwickelt sich eine Geschichte über Krieg, Konflikte, Liebe, Freundschaft und Freiheit, die mich tief gerührt hat.

Leider gelingt es der Autorin nicht, sich von Geschlechterklischees zu lösen. Es mag zwar nachvollziehbar sein, dass sich entsprechend geprägte und erzogene Charaktere entsprechend ihrer Rollenbilder verhalten, doch wird dies hier unnötig ausgebreitet. Abweichungen von den Klischees fehlen weitgehend. Abgesehen von einigen männlichen Charakteren, denen eine emotionale Seite zugesprochen wird, sind Männer stark, Frauen einfühlsam und die Aufgabenteilung wie im Mittelalter.

Trotz dieser Schwäche, die glücklicherweise nicht im Vordergrund der Handlung steht, kann ich die Lektüre von The Mount uneingeschränkt empfehlen.
