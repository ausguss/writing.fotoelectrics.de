+++
author = "Cord Riechelmann"
date = "2017-04-17"
description = "Gelungenes Portrait der Krähen- und Rabenvögel"
edition = "Berlin 2013"
hardcover = "yes"
img = "/img/reviews/riechelmann_kraehen.jpg"
isbn = "9783882210484"
language = "de"
pages = "156"
publisher = "Matthes und Seitz"
subtitle = "Ein Portrait"
title = "Krähen"

+++
Dieses Buch geht über die reine naturwissenschaftliche Darstellung der Krähen- und Rabenvögel hinaus, ergründet ihre Mythologie, ihr Verhalten, wie sie von uns Menschen wahrgenommen werden. Es handelt sich um eine sehr vielseitige, schön geschriebene Darstellung dieser Vögel und weckt Interesse und Sympathie für sie, ohne etwas zu beschönigen. Zum Abschluss werden noch zwanzig Krähenarten in Einzelportraits dargestellt. Die Aufmachung des Buches ist stilvoll und hochwertig und das Ganze ist sehr schön bebildert.
