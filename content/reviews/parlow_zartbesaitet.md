+++
author = "Georg Parlow"
date = "2017-02-25"
description = "Empfehlenswerte Auseinandersetzung mit dem Thema Hochsensibilität"
edition = "2. veränderte Auflage 2006"
hardcover = "no"
img = ""
isbn = "9783950176506"
language = "de"
pages = "247"
publisher = "Festland Verlag"
subtitle = "Selbstverständnis, Selbstachtung und Selbsthilfe für hochsensible Menschen"
title = "Zart besaitet"
+++
Dieses Buch hilft sehr, Hochsensibilität (HS) und damit einhergehende Besonderheiten in Fühlen, Wahrnehmung und Bedürfnissen zu verstehen. Es unterstützt bei der Ergründung, ob man selbst in die Gruppe der hochsensiblen Personen (HSP) fällt, und fördert Verständnis im Umgang mit Hochsensiblen.
Es wird sehr deutlich dargestellt, dass es sich bei HS nicht um eine Krankheit oder Behinderung handelt. Der Autor zeigt sowohl Chancen und positive Eigenschaften als auch Schwierigkeiten und Einschränkungen von HSP auf. Dabei wird deutlich, dass oft Probleme erst dadurch entstehen, dass unser Umfeld, beispielsweise am Arbeitsplatz oder im Einkaufszentrum, weitgehend von der nicht hochsensiblen Mehrheit gestaltet wurde und somit nicht auf die besondere Empfindsamkeit gegenüber Reizen, wie beispielsweise Lärm, von HSP angepasst ist.

Eine generelle Schwierigkeit bei der Ergründung, ob man selbst hochsensibel ist, ergibt sich daraus, dass die Ausprägung der HS von Person zu Person stark variiert. So wurde ich immer wieder verunsichert, wenn ich etwas über HS las, was nicht oder in anderer Form auf mich zutrifft. Dazu kam für mich die Angst, mir etwas anzumaßen und somit HSP ihre Besonderheit streitig zu machen. Diese Probleme sind allgemeiner Natur und nicht auf das vorliegende Buch bezogen. Ich möchte hier meine Erfahrung bei der Beschäftigung mit dem Thema teilen, dass es nicht zielführend ist, sich an einzelnen unzutreffenden Aspekten aufzuhängen, sondern mehr auf die Emotionen bei der Auseinandersetzung mit HS geachtet werden sollte. Es ist zu empfehlen, verschiedene Bücher zum Thema zu lesen, da jedes andere Aspekte betont und die Darstellung durch die Perönlichkeit der*des Autorin*Autors gefärbt ist. 

Mich hat die Lektüre von Zartbesaitet an vielen Stellen, insbesondere zum Thema Arbeit und Beruf, tief bewegt und ich konnte mich in vielem wiederfinden. So wurde für mich durch das Buch wesentlich klarer, dass ich hochsensibel bin. 
Ich habe es als sehr angenehm empfunden, dass der Autor nicht so stark auf das Esoterische zielt, da ich persönlich keinen besonderen Zugang dazu habe.

Ich freue mich, dass das Thema HS in den letzten Jahren an Aufmerksamkeit gewonnen hat. Die Zahl an HSP, die noch nichts von ihrer HS ahnen, ist groß. Ich hatte ebenfalls noch nicht lange vor der Lektüre zum ersten Mal von HS gehört. Dieses Buch leistet nach wie vor einen wertvollen Beitrag zur Aufklärung und zur Vergrößerung der Öffentlichkeit des Themas. Es wird stetig überarbeitet und aktualisiert, sodass es inzwischen in der 4. Auflage (2015) vorliegt. Für Hochsensible, solche, die sich diesbezüglich unsicher sind, solche mit HSP im persönlichen Umfeld, allgemein Interessierte und Personen, die beraterisch tätig sind, kann ich dieses Buch sehr empfehlen.
