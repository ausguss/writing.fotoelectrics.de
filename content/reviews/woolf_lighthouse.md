+++
author = "Virginia Woolf"
date = "2017-06-02"
description = "Einblick in das Leben einer Familie"
edition = "1. Auflage 1989"
hardcover = "no"
img = "/img/reviews/woolf_lighthouse.jpg"
isbn = "9780156907392"
language = "en"
pages = "209"
publisher = "Harcourt"
subtitle = ""
title = "To The Lighthouse"

+++
Wie auch schon bei 'Moments of Beeing', einer Sammlung autobiografischer Texte von Virginia Woolf, hatte ich zunächst Verständnisprobleme, habe mich dann aber im Laufe der Lektüre an den Schreibstil gewöhnt. Dennoch war ich hier und da unsicher bezüglich der verschiedenen Charaktere und wusste teils nicht, aus wessen Perspektive gerade geschildert wurde. Die Handlung konnte mich nicht besonders fesseln, doch manche der Schilderungen und Bilder haben mir gut gefallen. Woolf begibt sich mit viel Einfühlungsvermögen in ihre Charaktere. Der damit einhergehende Sexismus ist daher plausibel und ich vermute, dass aus ihm nicht die Stimme der Autorin spricht.

Ich habe keine größere Kritik an 'To the Lighthouse', war aber auch nicht begeistert. Mir liegt diese Art von Erzählung wohl einfach nicht besonders.
