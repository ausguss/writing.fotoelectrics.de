+++
author = "Jasper Fforde"
date = "2017-08-25"
description = "Farbenfrohe Dystopie"
edition = "2011"
hardcover = "no"
img = "/img/reviews/fforde_shades-of-grey.jpg"
isbn = "978-0-340-96305-0"
language = "en"
pages = "432"
publisher = "Hodder & Stoughton"
subtitle = ""
title = "Shades of Grey"

+++
Jasper Fforde sprudelt vor Ideen. In Shades of Grey verfolgt er wieder mal eine recht ungewöhnliche und denkt diese konsequent durch. In der Gesellschaft, die er zeichnet, haben die Menschen sehr unterschiedliche Farbwahrnehmungen. Die Farben, die sie wahrnehmen können, und in welcher Intensität sie dies vermögen, bestimmen ihre gesellschaftliche Stellung. Dieses System wird durch eine Vielzahl an Regeln gefestigt. Der junge Eddie Russett hat eigentlich rosige Aussichten und ist überzeugt, dass alles so seine Richtigkeit hat, doch bringt ihn seine Neugier immer wieder in Schwierigkeiten. Als er auf die verhaltensauffällige Jane aus der grauen Arbeiterschicht trifft, häufen sich diese Schwierigkeiten und er beginnt zu ahnen, dass etwas grundlegend faul ist.

Im Buch begegnet einem immer wieder Sexismus von Seiten der Charaktere, doch schreibe ich dies nicht dem Autor zu, der damit vielmehr ein plausibles Bild einer erschreckenden Gesellschaftsform abrundet, als diesen zu unterstützen.

Ich habe eine Weile gebraucht, um mich in die Geschichte zu finden. Die Charaktere haben mich nicht gleich vollends packen können. Doch ich wurde nach einer Weile in den spannenden Sog der Erzählung gezogen, als diese Fahrt aufnahm. Ich war neugierig darauf, was Eddie noch alles herausfinden und entdecken würde und wie er es schaffen würde, all seine Probleme zu jonglieren, ohne dass es für ihn im Desaster endet. Das Buch findet einen sinnvollen Abschluss, der Lust auf mehr macht, da vieles offen bleibt. Es ist, wie für Fforde üblich, der Auftakt zu einer Reihe, deren zweiter und dritter Teil unmittelbar angekündigt werden. Ob diese jemals erscheinen, halte ich jedoch für ungewiss. Ich persönlich, als begeisterter Fforde-Leser, hoffe, dass es zunächst mit einer der drei anderen Reihen des Autors weiter geht, da mich diese noch mehr gepackt haben.


