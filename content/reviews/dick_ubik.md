+++
author = "Philip K. Dick"
date = "2017-01-26"
description = "Science Fiction Verwirrspiel"
edition = "2004"
hardcover = "no"
img = "/img/reviews/dick_ubik.jpg"
isbn = "9781857988536"
language = "en"
pages = "224"
publisher = "Gollancz (Orion Publishing Group)"
subtitle = ""
title = "Ubik"

+++
In der in Ubik gezeichneten Zukunft werden Sterbende tiefgefroren, um sie möglichst lange für (seltene) Gespräche verfügbar zu halten. Einige Leute haben Psi-Kräfte, die von manchen für das Geschäft der Spionage und von anderen für das Geschäft der Spionageabwehr eingesetzt werden. Die Handlung ist bewusst verwirrend gehalten, da die Charaktere in ihrer eigenen Wahrnehmung gefangen sind und an der Realität zu zweifeln beginnen. Sie wirkt daher teils ziellos -- dies klärt sich gegen Ende aber auf. Stellenweise mutet die Erzählung albern an, wenn beispielsweise der Hauptcharakter mangels Kleingeld von der Münzeingangstür am Verlassen seiner Wohnung gehindert wird und dergleichen -- hier lässt sich auch Kapitalismuskritik vermuten, zumal dies gut zum Autor passt.

Es fällt auf, dass der Autor ausschließlich bei weiblichen Charakteren auf ihre Attraktivität hinweist, sie dabei auf ihr Äußeres reduziert und darauf, dass sie jung sind. Immerhin werden sie nicht als intellektuell oder körperlich unterlegen dargestellt. Es schien mir so, als ob der Charakter Al als Quotenschwarzer fungiert, aber immerhin ist er nicht klischeehaft dargestellt.

Insgesamt fand ich Ubik ganz interessant, ohne dass es mich wirklich überzeugen konnte. Es bietet die Möglichkeit, über Sterben und Tod, Menschsein und Rivalität, Freundschaft und Verbundenheit zu philosophieren, erzwingt dies aber nicht. Die Albernheiten haben mir Freude bereitet.
