+++
author = "Neil Gaiman"
date = "2017-05-04"
description = "Ein starkes Mädchen behauptet sich in einer bedrohlichen Welt"
edition = "2004"
hardcover = "no"
img = ""
isbn = "0060575913"
language = "en"
pages = "212"
publisher = "HarperTrophy (HarperCollins)"
subtitle = ""
title = "Coraline"

+++
Durch eine Tür tritt die junge Coraline in eine andere Version des Hauses, in dem sie wohnt. Dort warten ihre anderen Eltern, die scheinbar alles für sie tun würden, um sie bei sich zu behalten. Doch merkt Coraline sofort, dass das einen faden Beigeschmack hat und etwas daran abgrundtief falsch ist. Ein Kampf um ihre gewohnte Lebensrealität beginnt.

Nur hat mich die Geschichte zu keinem Zeitpunkt mitgerissen oder inspiriert. Mit Stilmitteln wie Ratten oder dem Essen von Insekten die Bosheit der anderen Mutter zu verdeutlichen, war für meinen Geschmack etwas zu klischeehaft. Zudem finden sich im Text immer wieder Andeutungen, die so offensichtlich sind, dass sie den weiteren Verlauf der Handlung vorwegnehmen. 

Die Illustrationen sind gelungen und fügen sich sehr gut in die Geschichte ein. Die Charaktere und ihr Verhalten sind ebenfalls weitgehend gelungen. Besonders positiv hervorzuheben ist Coraline als starke Heldin, die ohne Geschlechterklischees auskommt. Sie zeigt Mut und Verstand, ohne dabei unglaubwürdig zu wirken.
