+++
author = "Randall Munroe"
date = "2017-05-15"
description = "Unterhaltsam und lehrreich"
edition = "2015"
hardcover = "yes"
img = ""
isbn = "9781473620919"
language = "en"
pages = "61"
publisher = "John Murray (Hachette UK)"
subtitle = "Complicated Stuff in Simple Words"
title = "Thing Explainer"

+++
Der Thing Explainer enthält eine Sammlung liebevoll gezeichneter Skizzen zu verschiedensten Dingen, wie Spülmaschine, Uboot, dem menschlichen Körper, der Sonne und etlichen weiteren. 
Zur Beschreibung verwendet Munroe lediglich eine von ihm erstellte Liste der 1000 häufigsten englischen Worte (sein Vorgehen erläutert er am Ende des Buches). Durch diese Einschränkung werden die Erklärungen teils leicht verständlich, teils befremdlich und sehr oft lustig. 
Auch in den Zeichnungen selbst zieht sich ein dezenter Witz durch das gesamte Buch. Der Thing Explainer ist sicher kein Lehrbuch, macht jedoch Freude beim Lesen, Ansehen und Entdecken. Zudem ist er voller interessanter Fakten und Konzepte.
