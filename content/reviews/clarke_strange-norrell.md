+++
author = "Susanna Clarke"
date = "2017-04-12"
description = "Großartig, voller Witz, Spannung und Magie"
edition = "2007"
hardcover = "no"
img = ""
isbn = "9780747590057"
language = "en"
pages = "1026"
publisher = "Bloomsbury"
subtitle = ""
title = "Jonathan Strange & Mr Norrell"

+++
Dieser sorgfältig recherchierte Tatsachenbericht beschreibt, in seiner Zeit angemessener, gewählter Sprache und gefüllt mit Anmerkungen (die dem ganzen mehr historische Dichte verleihen), das Wiederaufleben der erloschenen britischen Magie durch Mr. Norrell und seinen Schüler Jonathan Strange zu Beginn des 19. Jahrhundert. Die beiden zeigen in ihrem sehr unterschiedlichen Wesen je Stärken und Schwächen und geraten durch ihre verschiedenen Ansichten bald in Konflikt. Es geht aber in dem Roman um viel mehr als diese beiden Magier. Es geht um Feen, Magie, Schicksalsschläge, das britische Gentlementum und die Gesellschaft, die aus damaliger Perspektive beschrieben, aber dennoch mit viel Fingerspitzengefühl und Ironie indirekt kritisiert wird. So wird deutlich, dass Frauen, Untergebene und Ausländer, trotz ihrer gesellschaftlichen Stellung, zu viel mehr fähig sind, als ihnen in ihrer Rolle zugemutet wird, während die feinen Gentlemen sich mit vollstem Recht und vollster Überzeugung vielfältige Verfehlungen leisten. Leider versäumt es die Autorin dennoch, auch nur einen weiblichen Charakter grundlegend etwas zur Handlung beitragen zu lassen und zeigt Frauen ausschließlich in der Opferrolle.
