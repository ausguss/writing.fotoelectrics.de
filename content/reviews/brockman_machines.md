+++
author = "John Brockman (Hrsg.)"
date = "2017-03-23"
description = "Buntes Sammelsurium verschiedenster Ideen und Denkansätze zum Thema Künstliche Intelligenz"
edition = "2015"
hardcover = "no"
isbn = "9780062425652"
language = "en"
pages = "576"
publisher = "Harper Perennial (HarperCollins)"
subtitle = "Today's Leading Thinkers on the Age of Machine Intelligence"
title = "What to Think About Machines That Think"
img = ""

+++
Ich habe mich mit dem Buch anfangs schwer getan, das Interesse verloren und es beiseite gelegt, bis ich erkannt habe, dass es sich hervorragend als Klolektüre eignet.

Frage ein paar hundert kluge Köpfe nach künstlicher Intelligenz und erhalte ein paar hundert unterschiedliche Antworten. Allein aus dieser Tatsache lässt sich schon das ein oder andere ableiten (-- die Frage ist, zugegeben, auch bewusst vage gehalten).

So zeigt sich bereits eine große Unklarheit darüber, was Intelligenz ausmacht, was mit Denken gemeint ist und um was für eine künstliche Intelligenz es sich überhaupt handeln soll. In dieses Wirrwar mischen sich Fragen nach Bewusstsein, der Motivation des Denkens (Fragestellung), dem Zusammenspiel von Körper und Bewusstsein und viele weitere.

Trotz der Erkenntnis, dass die Definition von Intelligenz, als das was mit einem Intelligenztest gemessen wird, sehr beschränkt und problematisch ist, wird gemeinhin weiter krampfhaft an dieser festgeklammert. In diesem Buch wird der Begriff von Essay zu Essay unterschiedlich weit gefasst oder diskutiert.

Diese Schwammigkeit der Begriffe zeigt, wie schwierig das Thema zu greifen ist. Für mich scheint die Frage nach dem Für und Wider (bzw. den Gefahren und Möglichkeiten) einer KI mit eigenem Bewusstsein, wie sie in zahlreichen Science Fiction Szenarien gezeichnet wird, verfrüht (eine solche zeichnet sich bisher nicht einmal ansatzweise am fernen Wissenschaftshorizont ab). Es gibt wesentlich dringlichere Probleme und Fragestellungen, auf die sich unser Augenmerk derzeit richten sollte -- auch und gerade im Bezug auf Computersysteme und deren Einfluss auf die Gesellschaft. Diese Meinung lässt sich übrigens ebenfalls in der Essaysammlung finden. Es finden sich zudem Stimmen, die vor durchaus realen akuten Problemen warnen.

Für mich war auch eine Darlegung sehr überzeugend, dass es sich bei den aktuell fortschrittlichsten KI-Systemen nicht um tiefgreifende Umsetzungen neuer Erkenntnisse handelt, sondern lediglich um die (geschickte) Implementierung alter Algorithmen auf neuer, schnellerer Hardware.

Man bekommt also ziemlich viele verschiedene Denkanstöße, die teils auf andere Themengebiete (z.B. Psychologie, Philosophie, Medizin) abfärben. Die Sammlung ist (nicht nur deshalb) sehr interessant. Seinen Gedanken freien Lauf zu lassen und sich mit Themen zu beschäftigen, die zunächst keinen erkennbaren Nutzen haben, hat durchaus etwas positives. Daraus erwächst oft ungeahnt etwas nützliches und die Freude am Thema ist ebenfalls ein vertretbarer Nutzen. Wichtig ist dabei nur, nicht in unbegründete Panik zu verfallen oder der Niederkunft des künstlichen Messiahs zu frohlocken.
