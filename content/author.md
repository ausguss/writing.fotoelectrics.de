+++
date = "2015-09-26T13:55:06+02:00"
draft = false
title = "Autor"
metadescription = "Josa Wode schreibt Geschichten. Er schreibt auch Selbstbeschreibungen wie diese. Wer weiß, was er noch schreiben wird?"
+++
<img class="autoscaled leftfloat" alt="Der Autor Josa Wode bei seiner täglichen Recherche (Photo: Tom Meng)" src="../img/josa-wode.png" style="margin-top:12px;" />
Der Autor ist Josa Wode. 
Josa Wode ist eine diesweltliche Entität 1985er Jahrgangs (unserer Zeitrechnung) auf der Suche nach einer besseren Realität. 
Ob es einen Unterschied zwischen Traum- und Wachrealität gibt, ist dabei noch abschließend zu klären. 
Wahrscheinlich aber doch.

Der Autor versucht, bestehende Verhältnisse zu durchbrechen und zu etwas Besserem zu wandeln. 
Dabei fängt er bei sich selber an (der alte Egoist).

Bestehende Verhältnisse und etwas Besseres sind schöne schwammige Begriffe, die jede\*r nach eigenen Vorstellungen biegen und drehen kann. 
In diesem Fall zielt der Begriff *Bestehende Verhältnisse* auf Macht- und Herrschaftsverhältnisse jedweder Art, Grenzen, Rassismus (ja, auch Antisemitismus), Sexismus, Homophobie ... (guter Trick, so kann niemand sagen, ich hätte was vergessen) und *etwas Besseres* auf die Abschaffung all dessen in den Köpfen und überall sonst, auf anarchistische/basisdemokratische Organisation, auf Selbstbestimmung und Solidarität und solchen Kram (uns wird schon noch was einfallen). 
Der Autor fühlt sich nicht wohl dabei, mit solch großen Worten um sich zu werfen, hat aber derzeit keine besseren oder bescheideneren parat.

Der Autor ist Autor, weil er Autor sein möchte. 
Seinem Interesse, mit Gegenständen zu kommunizieren ([Softwareentwicklung, Programmierung](https://coding.josawode.de)), geht er zum Spaß und auch für den Lebensunterhalt nach. 
Zudem hat er einen Blick in die andere Richtung gewagt und sich gefragt, wo all das Papier für die vielen Bücher eigentlich herkommt. 
So geht er nebenbei der [Baumpflege](https://www.astronaut-baumpflege.de) nach (Bäume möglichst lange stehen lassen ist eben auch schön) und kann ruhigen Gewissens dafür sorgen, dass viele weitere Bäume zu Büchern werden.

Der Autor wird Autor bleiben, solange er den Kopf/Rücken dafür frei hat. 
Wer das ebenfalls möchte, ist eingeladen dies zu [unterstützen](../support/). 
