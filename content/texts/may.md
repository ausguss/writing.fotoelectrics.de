+++
date = "2018-05-01"
writers = "Josa Wode"
description = "Ein unwiderstehlicher Ruf"
publishlicense = "Creative Commons BY-NC-SA 3.0"
metatype = "Eine kurze Geschichte"
metadescription = ""
title = "Mai"

nosupportinfo = "no"
+++
Dies ist eine kurze Maigeschichte. Sie ist Teil des als Taschenbuch erhältlichen Sammelbandes [Leben, Träumen und Sterben](../live-dream-die/). Bis zum Jahresende wird zudem jeden Monat eine weitere Geschichte aus der Reihe auf meiner Seite erscheinen.

#### Zum Inhalt

*Horror, Verzweiflung*  
Ein Ziegenhirte gerät in den Strudel unvorstellbarer Ereignisse.

#### Lesen
[html](../../documents/may/de/html/) - [pdf](../../documents/may/de/pdf/mai_josa-wode.pdf)

**Druckempfehlung**: Das Format des Pdf-Dokuments ist DIN A5, daher sollten zwei Seiten auf eine (A4-)Seite gedruckt werden (und natürlich beidseitig, um Papier zu sparen).  
[Anleitung zum Broschürendruck](../tutorial-broschure-printing/)

#### Änderungen
27.01.2018 -- Korrigierte Erstfassung



