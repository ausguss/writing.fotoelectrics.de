+++
date = "2021-11-18"
writers = "Josa Wode"
description = "An der Küste"
publishlicense = "Creative Commons BY-NC-SA 3.0"
metatype = "Eine kurze Geschichte"
metadescription = ""
title = "November"

nosupportinfo = "no"
+++
Diese kurze Novembergeschichte ist Teil des als Taschenbuch erhältlichen Sammelbandes [Leben, Träumen und Sterben](../live-dream-die/).

#### Zum Inhalt

*Fantasie, Tapferkeit*  
Ein Kind stellt sich gegen Wind und altes Unrecht.

#### Lesen
[html](../../documents/november/de/html/) - [pdf](../../documents/november/de/pdf/november_josa-wode.pdf)

**Druckempfehlung**: Das Format des Pdf-Dokuments ist DIN A5, daher sollten zwei Seiten auf eine (A4-)Seite gedruckt werden (und natürlich beidseitig, um Papier zu sparen).  
Wie man eine Broschüre druckt (und bastelt) erkläre ich in [dieser Anleitung](../tutorial-broschure-printing/).

#### Änderungen
07.01.2018 -- Korrigierte Erstfassung



