+++
date = "2017-12-03T14:45:30+01:00"
description = "Ein ermutigendes Buch zu einem schweren Thema"
metadescription = ""
metatype = "Kinderbuch"
publishlicense = ""
title = "Rena von den hohen Häusern"
writers = "Nuala Huther"

bookinfo = "yes"
publisher = "Books on Demand"
edition = "2017"
hardcover = "no"
isbn = "9783746036793"
language = "de"
pages = "36"
img = "/img/texts/huther_rena.jpg"

nosupportinfo = "yes"
+++

Rena lebt mit ihrem Vater in den hohen Häusern. Leider geht es ihm nicht gut, seit Renas Mutter gestorben ist. 

Doch sie lässt sich nicht unterkriegen, erlebt auch Freundschaft und Zuversicht. Und es gibt Veränderungen, die neue Hoffnung mit sich bringen.



Die Erzählung ist ergreifend und feinfühlig formuliert. Sowohl Text als auch Bilder haben mich beim Lesen bewegt -- teils sind es kleine Details, die mir nahe gehen.

Die rohen, ausdrucksstarken Illustrationen spiegeln das Imperfekte und Provisorische des Lebens wieder. Sie zeigen, dass es in großen, grauen Kästen auch Farbe und Leben gibt und unterstreichen, dass sich scheinbar in Widerspruch zueinander Stehendes, wie Trauer und Freude, nicht gegenseitig ausschließt. Dass sich die Abbildungen jenseits enger Geschlechterklischees bewegen, empfinde ich als wohltuenden Kontrast zur leider all zu häufig (nicht nur) in Kinderbüchern wiedergekäuten, beschränkten Hellblau-Rosa-Dualität.



Ich halte es für wichtig, dass Kinder -- auf einfühlsame Weise -- auch an schwierige Themen herangeführt werden. Gerade Depression ist etwas, das uns überall begegnen kann und viele Menschen betrifft. Daher freue ich mich über dieses Buch, das einen einfachen Zugang zum Thema bietet und Betroffene und Angehörige unterstützen kann, sich damit weniger allein gelassen zu fühlen.



Ich möchte eine gewisse Befangenheit bei dieser Buchvorstellung nicht abstreiten, da ich der Autorin mit dem Layout geholfen habe und ihr auch sonst recht nahe stehe. Daher findet sich dieser Beitrag auch nicht bei meinen sonstigen [Rezensionen](/reviews/). Eine Leseempfehlung gebe ich aber trotzdem.



Und hier kann das Buch bestellt werden: [BoD -- Books on Demand](https://www.bod.de/buchshop/rena-von-den-hohen-haeusern-nuala-huther-9783746036793)
