+++
date = "2018-03-01"
description = "kurze Geschichten rund ums Jahr"
metadescription = ""
metatype = "Sammelband kurzer Geschichten"
publishlicense = ""
title = "Leben, Träumen und Sterben"
writers = "Josa Wode"

bookinfo = "yes"
subtitle = "kurze Geschichten rund ums Jahr"
publisher = "Books on Demand"
edition = "2018"
hardcover = "no"
isbn = "9783746097282"
language = "de"
pages = "92"
img = "/img/texts/live-dream-die.png"
ogimage = "/img/texts/live-dream-die_fb.png"

nosupportinfo = "yes"
+++
Diese Geschichtensammlung führt mit einer kurzen Geschichte zu jedem Monat durch das Jahr. Das Geschilderte reicht von Spannung und Horror zu Düsterromantik und Träumereien.  
Von realistisch bis phantastisch werden Tode gestorben und Leben gelebt.  

Zudem ist die märchenhafte Erzählung *Kommt und holt sie* enthalten:  
Das Verschwinden einer Prinzessin versetzt einen ganzen Königshof in Aufruhr. Die fähigsten Recken des ganzen Landes werden entsandt, die verschollene Königstochter Karina aus den Fängen einer scheinbar bösartigen Hexe zu befreien.  
Doch was hält die eigensinnige Karina von der ganzen Angelegenheit? Wie ergeht es ihr am Königshof und was erlebt sie bei ihrem Verschwinden?  

Einen tieferen Einblick ermöglichen die auf meiner Webseite veröffentlichten Geschichten.

Hier kann das Buch bestellt werden: [BoD -- Books on Demand](https://www.bod.de/buchshop/leben-traeumen-und-sterben-josa-wode-9783746097282)
