+++
date = "2018-09-02"
writers = "Josa Wode"
description = "Pfade der Erinnerung"
publishlicense = "Creative Commons BY-NC-SA 3.0"
metatype = "Eine kurze Geschichte"
metadescription = ""
title = "September"

nosupportinfo = "no"
+++
Dies ist eine kurze Septembergeschichte. Sie ist Teil des als Taschenbuch erhältlichen Sammelbandes [Leben, Träumen und Sterben](../live-dream-die/). Bis zum Jahresende wird zudem jeden Monat eine weitere Geschichte aus der Reihe auf meiner Seite erscheinen.

#### Zum Inhalt

*Bewältigung, Surreales*  
Moira stellt sich auf einer Wanderung einem unangenehmen Ereignis ihrer jüngeren Vergangenheit. Wohin verschlägt es sie dabei?

#### Lesen
[html](../../documents/september/de/html/) - [pdf](../../documents/september/de/pdf/september_josa-wode.pdf)

**Druckempfehlung**: Das Format des Pdf-Dokuments ist DIN A5, daher sollten zwei Seiten auf eine (A4-)Seite gedruckt werden (und natürlich beidseitig, um Papier zu sparen).  
Wie man eine Broschüre druckt (und bastelt) erkläre ich in [dieser Anleitung](../tutorial-broschure-printing/).

#### Änderungen
07.01.2018 -- Korrigierte Erstfassung



