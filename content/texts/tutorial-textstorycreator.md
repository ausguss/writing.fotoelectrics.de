+++
date = "2018-05-18"
description = "Anleitung zur Textsetzung für Geschichten – einfach und hochwertig"
metadescription = ""
metatype = "Tutorial"
publishlicense = "Creative Commons BY-NC-SA 3.0"
title = "Textstory Creator Tutorial"
writers = "Josa Wode"

img = "/img/textstorycreator.png"
+++
<img alt="Textstory Creator" src="/img/textstorycreator.png" class="autoscaled rightfloat"/>

### Ausgangslage 

Du hast eine Geschichte geschrieben voller Pirat\*innen, verborgener Schätze und aufregender Seeschlachten. Nun möchtest du sie drucken und/oder im Internet präsentieren. Dein Anspruch diktiert, dass dabei eine hochwertige Webansicht bzw. ein hochwertiges Pdf-Dokument entsteht, um den Leuten beim Lesen möglichst viel Freude und Lesekomfort zu bieten. Die Geschichte soll schließlich im besten Licht präsentiert werden.  

Allerdings bist du in erster Linie am Schreiben interessiert und nicht daran, in die Tiefen professioneller Textsetzung einzutauchen. In diesem Bereich wünschst du dir &ndash; in Ermangelung anderer, die für dich perlentauchen &ndash; Schwimmflügel mit denen du dich im seichten Wasser vergnügen kannst.

Als Schwimmflügel (oder Treibgut oder was auch immer) dient in diesem Tutorial der [Textstory Creator](https://bitbucket.org/ausguss/textstory_creator/).
Dieses Programm unterstützt dich beim Layouten deines Textes. Anhand einer einfachen *Markupsprache* (was das ist und wie es funktioniert, wird weiter unten erklärt) kannst du deinen Text formatieren. Im *Textstory Creator* kannst du noch Einstellungen zum Layout vornehmen und ein paar allgemeine Angaben (wie Titel oder Autor\*in) machen. Auf Knopfdruck wird dann deine fertige Geschichte ausgespuckt.

Falls du an der Entstehung des *Textstory Creator* interessiert bist, findest du [hier](https://coding.josawode.de/projects/textstorycreator/) ein paar Informationen auf Englisch.

### Seekarte zum Tutorial

* [Vorbereitungen](#vorbereitungen) -- Auswahl und Installation der nötigen Werkzeuge
* [Wir stechen in See](#wir-stechen-in-see) -- Anlegen eines neuen *Textstory*-Projekts und Vornehmen der Einstellungen
* [Der Inhalt](#inhalt) -- Die Rohfassung der Geschichte
* [Text gestalten](#text-gestalten) -- Der Kern des Ganzen: Das Layouten der Geschichte
* [Lizenz](#lizenz) -- Informationen zum Anpassen der Lizenz unter der die Geschichte veröffentlicht werden soll
* [Konvertieren](#konvertieren) -- So kommst du zum fertigen Ergebnis
* [Auf zu neuen Ufern](#auf-zu-neuen-ufern) -- Ein paar weiterführende Informationen
* [Immer eine Handbreit Wasser unter dem Kiel](#wasser-unter-dem-kiel) -- Schlusswort

### <a id="vorbereitungen"></a>Vorbereitungen

#### TeX Live

Du installierst [TeX Live](http://tug.org/texlive/), falls es nicht schon vorhanden ist:

* *Debian/Ubuntu* &ndash; Du öffnest ein Terminalfenster und gibst `sudo apt-get install -y texlive` ein.
* *Windows* &ndash; Du lädst den [TeX Live Installler](http://mirror.ctan.org/systems/texlive/tlnet/install-tl-windows.exe) herunter, startest ihn mit Doppelklick und folgst den Anweisungen.

*TeX Live* wird später benötigt, um aus dem generierten *LaTeX*-Projekt ein fertiges *Pdf*-Dokument zu erzeugen. Falls du nicht weißt, was das bedeuten soll, macht das gar nichts. Installiere einfach *TeX Live* und finde später heraus, was das Ganze soll.

#### Textstory Creator

Du installierst den *Textstory Creator* entsprechend des Abschnitts [Installation](https://bitbucket.org/ausguss/textstory_creator/src/master/README.md#markdown-header-installation) der Anleitung.

#### Texteditor

Entweder hast du schon einen Lieblings-Texteditor oder du entscheidest dich nun, welchen du benutzen möchtest. Die Auswahl ist groß. Ein paar Vorschläge:

*Plattformübergreifend*

* [Atom](https://atom.io/) &ndash; für Weltraumpirat\*innen
* [Sublime Text](https://www.sublimetext.com/) &ndash; für jedes Enterkommando
* [Vim](https://www.vim.org/download.php) &ndash; mächtig, aber einarbeitungsintensiv; nichts für Süßwassermatros\*innen

*Unix*

* [Kate](https://kate-editor.org/) &ndash; einfache, wendige Fregatte

*Windows*

* [Notepad++](https://notepad-plus-plus.org/) &ndash; komfortable und mächtige Galeone

Nicht geeignet sind solche Produkte wie *Open Office*, *Libre Office* oder *MS Word*, da diese keine reinen Textdateien speichern.

### <a id="wir-stechen-in-see"></a>Wir stechen in See

#### Projektverzeichnis

Du legst dein Projektverzeichnis (z.B. unter deinem *Home*-Verzeichnis oder im *Eigene Dateien*-Ordner) an. (Erstelle einfach einen leeren Ordner mit einem Namen deiner Wahl.)

![Projektverzeichnis](../../img/tutorial-textstorycreator/project-directory.png)

#### Projekt Laden im *Textstory Creator*

Nun startest du den *Textstory Creator*. In der rechten oberen Ecke kannst du die Sprache der Anwendung wechseln.

Du befindest dich bereits auf der Seite mit den Pfadeinstellungen. Dort ziehst du dein neues Projektverzeichnis aus deinem *Dateibrowser* bei gedrückter Maustaste auf den Bereich mit der Überschrift *Ausgabeverzeichnis per Drag&Drop festlegen*. 

Nun wirst du gefragt, ob folgende Dateien angelegt werden sollen:

* *textstory.txt* &ndash; für deine mit *Textstory Markup* formatierte Geschichte
* *setup.toml* &ndash; für allgemeine Angaben zur Geschichte und diverse Einstellungen

Bestätige dies. 

Alternativ kannst du auch Dateien aus anderen Pfaden wählen oder andere Dateinamen wählen. Weitere Hinweise zum Festlegen der Pfade findest du im Abschnitt [Pfade festlegen](https://bitbucket.org/ausguss/textstory_creator/src/master/README.md#markdown-header-pfade-festlegen) der Anleitung.

![Pfade angeben](../../img/tutorial-textstorycreator/paths-setup.png)

#### Einstellungen vornehmen

Nachdem dies erledigt ist, klickst du auf den Reiter *Textstory Einstellungen* und machst die passenden Angaben für deine Geschichte. Klicke einfach auf die jeweilige Einstellung, um sie zu ändern. Bei gedrückter Maustaste kannst du den Bereich nach oben ziehen oder du scrollst mit dem Mausrad, um zu den weiter unten befindlichen Einstellungen zu gelangen.

*Allgemeine Einstellungen*

* *Titel* &ndash; Abenteuer in der blutigen See
* *Untertitel* &ndash; Schatzsuche auf der Totenkopfinsel
* *Autor\*in* &ndash; Josa Wode
* *Sprache* &ndash; de

*LaTeX*

* *Autor\*in im Titel* &ndash; Ja *(Um diese optionale Einstellung zu aktivieren, klickst du auf das Kästchen zu ihrer Linken.)*
* *Inhaltsverzeichnis* &ndash; Ja
* *Titel des Inhaltsverzeichnis* &ndash; Navigieren in der blutigen See *(Diese Einstellung ist hauptsächlich für Geschichten in anderen Sprachen gedacht, kann aber auch wie hier atmosphärisch eingesetzt werden.)*
* *Kapitel Seitenumbruch* &ndash; Nein
* *Pdf-Thema* &ndash; Pirat\*innengeschichte
* *Pdf-Schlüsselworte* &ndash; Geschichte, Piraterie, Seefahrt, Abenteuer

*HTML*

* *Metabeschreibung* &ndash; Abenteuer in der blutigen See \-\- Schatzsuche auf der Totenkopfinsel \-\- eine Geschichte von Josa Wode
* *Regionalangabe* &ndash; de_DE
* *URL* &ndash; https://josawode.de/documents/pirates-in-the-sea-of-blood/de/html/ (Hier wird die HTML-Version der Geschichte später zu finden sein.)
* *Sitename* &ndash; https://josawode.de

![Einstellungen vornehmen](../../img/tutorial-textstorycreator/settings-tab.png)

Würdest du deine (noch leere) Geschichte nun konvertieren (dazu komme ich später), sähe das wie folgt aus:

[html](../../documents/pirates-in-the-sea-of-blood/de/html/index-empty.html) - [pdf](../../documents/pirates-in-the-sea-of-blood/de/pdf/abenteuer-in-der-blutigen-see_josa-wode_empty.pdf)

Dabei dürfte auffallen, dass neben der zu erwartenden Titelzeile am Ende des Dokuments eine Lizenz angefügt wurde. Diese kann übernommen werden, lässt sich aber auch anpassen oder entfernen. Mehr dazu später.

Zudem können bei genauerer Untersuchung die gemachten Metaangaben betrachet werden:

*Pdf-Eigenschaften*  

![Pdf-Metadaten](../../img/tutorial-textstorycreator/pdf-properties.png)

*Auszug aus den HTML Metadaten* 
 
```
<head>
...
<title>Abenteuer in der blutigen See | Josa Wode</title>
<meta name="author" content="Josa Wode">
<meta name="description" content="Abenteuer in der blutigen See -- Schatzsuche auf der Totenkopfinsel -- eine Geschichte von Josa Wode">
...
<meta property="og:url" content="https://josawode.de/documents/pirates-in-the-sea-of-blood/de/html/" />
<meta property="og:locale" content="de_DE" />
<meta property="og:site_name" content="https://josawode.de" />
...
<meta property="og:title" content="Abenteuer in der blutigen See" />
<meta property="og:description" content="Abenteuer in der blutigen See -- Schatzsuche auf der Totenkopfinsel -- eine Geschichte von Josa Wode" />
...
</head>
```

Das ist ja schon mal ein Anfang, doch jetzt gilt es, das Ganze mit Inhalt zu füllen.

### <a id="inhalt"></a>Der Inhalt

Du öffnest deine *textstory.txt* (aus dem Verzeichnis, das du zuvor angegeben hast) mit einem Texteditor deiner Wahl und legst los. Vermutlich hast du die Geschichte zuvor, wie es sich für echte Pirat\*innen gehört, mit Federkiel und Tinte auf Büttenpapier festgehalten. Nun ist es an der Zeit, sie in das Zeitalter der Softwarepiraterie zu übertragen.

```
Kapitel 1: Irgendwo im Nirgendwo

Seit drei Wochen sind wir nun schon auf hoher See ohne auch nur eine Landlinie am Horizont. Das Wasser ist inzwischen brackig und ich trinke es nur noch mit verschlossenen Augen, um die kleinen Würmchen darin besser ignorieren zu können. Zum Glück habe ich noch Rum, um den Geschmack zu vertreiben. Unser Essen ist zwar auch noch lange nicht knapp, doch es schmeckt von Tag zu Tag schlechter und abwechslungsreich ist es schon lange nicht mehr.

Vier Tage Flaute...
```

([Hier](../../documents/pirates-in-the-sea-of-blood/de/src/textstory-raw.txt) kannst du dir den vollständigen Text ansehen.)

Nun sieht die Sache so aus:
[html](../../documents/pirates-in-the-sea-of-blood/de/html/index-raw.html) - [pdf](../../documents/pirates-in-the-sea-of-blood/de/pdf/abenteuer-in-der-blutigen-see_josa-wode_raw.pdf)

### <a id="text-gestalten"></a>Text gestalten

Das ist ja schon mal ganz annehmbar, aber stellt deine hohen Ansprüche natürlich noch nicht zufrieden, also machst du dich daran, der Sache Form zu verleihen. 
Im Folgenden wirst du das *Textstory Markup* kennenlernen -- einfache Befehle mit denen du deinen Text formatieren (beispielsweise fett oder kursiv markieren) kannst.
(Bist du erst einmal mit dem *Textstory Creator* vertraut, wirst du diese Schritte vermutlich gleich beim Schreiben oder Übertragen der Geschichte in deine *textstory.txt* anwenden, um dir Arbeit zu ersparen.)

#### Schon stimmig

Folgende Punkte werden bereits wie gewünscht dargestellt. Hier siehst du, warum das so ist.

*Wörtliche Rede*

Es dürfte aufgefallen sein, dass die *Anführungszeichen oben* des Ausgangstextes in französische Anführungszeichen (die sogenannten *Guillemets* in deutscher Verwendung) übertragen wurden.

Beispiel:  
*Markup:* "Alles in Ordnung mit dir? Siehst etwas mitgenommen aus."    
*Ergebnis:* »Alles in Ordnung mit dir? Siehst etwas mitgenommen aus.«

*Gedankenstriche*

Durch zwei aufeinander folgende Minuszeichen (Bindestrich-Minus, um genau zu sein) wird im Eingangstext der längere Halbgeviertstrich (für Gedankenstriche und Spiegelstriche) angedeutet.

Beispiel:  
*Markup:* Doch wo sonst könnte ich \-\- geboren ohne das erforderliche Zubehör in der Unterbekleidung \-\- meiner Leidenschaft nachgehen.  
*Ergebnis:* Doch wo sonst könnte ich &ndash; geboren ohne das erforderliche Zubehör in der Unterbekleidung &ndash; meiner Leidenschaft nachgehen.

*Absatztrennung*

Es ist leicht zu erkennen, dass ein Zeilenumbruch im *Textstory*-Markup zu einem Absatz führt und eine Zeile Durchschuss als eben solche interpretiert wird.

#### Weitere Gestaltung

*Kapitel*

Das Inhaltsverzeichnis ist leer und die Kapitelüberschriften sehen nicht besonders beeindruckend aus.
Im *Textstory-Markup* werden Überschriften mit mit \#\# markiert.

Beispiel:  
*Markup:* \#\# Kapitel 1: Irgendwo im Nirgendwo  
*Ergebnis:*
![Inhaltsverzeichnis und erste Kapitelüberschrift](../../img/tutorial-textstorycreator/headlines.png)

*kursiver Text*

Text zwischen Sternchen wird *kursiv* gesetzt.

Beispiel:

*Markup:*  
\*Fahre ein ins Maul des Todes sacht  
Empor an Messers Schneide  

Am toten Baum der Affen Acht  
Wo ist die Hand aus Kreide  

Nimm Rat bei dem der Ehrfurcht hat  
Find glänzendes Geschmeide\*  

*Ergebnis:*  
*Fahre ein ins Maul des Todes sacht*  
*Empor an Messers Schneide*  

*Am toten Baum der Affen Acht*  
*Wo ist die Hand aus Kreide*  

*Nimm Rat bei dem der Ehrfurcht hat*  
*Find glänzendes Geschmeide*  

*fetter Text*

Text zwischen doppelten Sternchen wird **fett** gedruckt.

Beispiel:  
*Markup:* Auf der alternativen Route war eine Stelle mit einem \*\*X\*\* versehen.  
*Ergebnis:* Auf der alternativen Route war eine Stelle mit einem **X** versehen.

*Bilder*

Es können auch Bilder in die Textstory eingebettet werden.

Beispiel:  
*Vorbereitungen:*  
Die Bilddatei *secret-message.png* wird mit der gewünschten Bildgröße erstellt und im Textstory Ausgabeverzeichnis in die Unterordner *html/img* und *latex/img* gelegt.  
*Markup:*   
!\[Verborgener Hinweis\](img/secret-message.png)  
*Ergebnis:*  
![Verborgener Hinweis](../../img/tutorial-textstorycreator/secret-message.png)

### <a id="lizenz"></a>Lizenz

#### Allgemeines

Bei jeglicher Veröffentlichung ist es wichtig, sich über die Rechte Gedanken zu machen. 
Das bedeutet nicht nur, dass man die entsprechenden Rechte zur Verwendung des Materials Anderer benötigt und gegebenenfalls entsprechende Angaben macht, sondern auch, dass man sich überlegen sollte, wie man eigenes Material anderen zugänglich machen möchte und dabei seine eigenen Interessen (und/oder die der Gemeinschaft) schützt.

Dies ist ein sehr weites Thema und es gibt viele verschiedene Meinungen dazu. Ich werde hier nicht vertiefend darauf eingehen. Aber bitte informiere dich und suche dir das für dich Passende heraus.

Ich habe mich bei meinen bisherigen Textveröffentlichungen für eine [Creative Commons Lizenz vom Typ Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen](http://creativecommons.org/licenses/by-nc-sa/3.0) entschieden.
Creative Commons bietet auch [andere Lizenzen](https://creativecommons.org/share-your-work/), zudem gibt es etliche weiterer Lizenzen von anderen Organisationen.

Lässt man Lizenz- und Urheberrechtsangaben ganz weg, so gilt in Deutschland (und den meisten anderen Ländern außer den USA) automatisch das Urheberrecht. Im Detail ist die Sache dann aber, denke ich, doch etwas komplizierter.

Ich bin kein Rechtsexperte und gebe keine Gewähr für hier gemachte Angaben.

#### Lizenzen im Textstory Creator

Die von mir im vorigen Abschnitt erwähnte Lizenz [CC BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0) ist standardmäßig in Dokumenten, die mit dem *Textstory Creator* generiert wurden, enthalten. Solltest du dich für diese Lizenz entscheiden, brauchst du nichts weiter zu tun.

Das Ändern der Lizenzen ist derzeit nur global und nicht in den einzelnen Projekten möglich. Die Lizenzen für *Html* und *LaTeX* werden gesondert behandelt. Es kann also auch jeweils eine andere Lizenz verwendet werden.

Global meint, dass eine Anpassung im Installationsverzeichnis des *Textstory Creator* gemacht wird und für alle (zukünftigen) Projekte relevant ist. Bei Debian-Systemen ist das <a id="licenses-installdir"></a>Installationsverzeichnis üblicherweise */opt/textstorycreator*, bei Windows *C:/Program Files (x86)/Textstory Creator*.

*Html*

Die Lizenz findest du im Installationsverzeichnis unter *textstory_converter/html/license.tpl.html*. 

Durch Umbenennen oder Löschen dieser Datei sorgst du dafür, dass beim Konvertieren deines Werkes keine Lizenz in der fertigen *Html*-Version erscheint.

Du kannst sie auch austauschen oder bearbeiten. Das Erscheinungsbild wird durch die *CSS-Stylesheets* in *textstory_converter/html/css/license-styles.css* bestimmt.

Unter *textstory_converter/html/templates/licenses* findest du die Standardlizenz. Solltest du eine andere Lizenz erstellen, freue ich mich, diese als Vorlage aufzunehmen (<a href="&#x6d;&#x61;&#x69;&#x6c;&#x74;&#111;&#x3a;&#99;&#x6f;&#100;&#x69;&#x6e;&#103;&#x40;&#102;&#111;&#116;&#111;&#101;&#x6c;&#101;&#x63;&#116;&#x72;&#105;&#99;&#x73;&#x2e;&#x64;&#x65;">Email</a>).

*LaTeX*

Auch hier findest du die Lizenz im Installationsverzeichnis, diesmal allerdings unter *textstory_converter/latex/license.tex*. 

Genau wie bei der *Html*-Version kann diese durch Umbenennen oder Löschen entfernt werden.

Ein Bearbeiten oder Austauschen durch eine andere Lizenzvorlage ist ebenfalls möglich.

Die Standardlizenz liegt unter *textstory_converter/latex/templates/licences*. Bitte schicke mir auch hier von dir erstellte Vorlagen, damit ich sie in die Auswahl aufnehmen kann und andere etwas davon haben (<a href="&#x6d;&#x61;&#x69;&#x6c;&#x74;&#111;&#x3a;&#99;&#x6f;&#100;&#x69;&#x6e;&#103;&#x40;&#102;&#111;&#116;&#111;&#101;&#x6c;&#101;&#x63;&#116;&#x72;&#105;&#99;&#x73;&#x2e;&#x64;&#x65;">Email</a>).

### <a id="konvertieren"></a>Konvertieren

Du bist so weit. Nun kannst du zum *Konvertieren*-Reiter wechseln und per Knopfdruck die Konvertierung starten. Setze das Häkchen bei *pdf erstellen*, um die Umwandlung von *LaTeX* zu *Pdf* durchzuführen -- dafür muss *TeX Live* installiert und etwas Geduld vorhanden sein.

Im unteren Bereich kann der Konvertierungsvorgang nachvollzogen werden. Geht etwas schief, findest du hier wertvolle Informationen. Durch ziehen mit der Maus kannst du dich durch die vollständige Konsolenausgabe bewegen.

![Konvertieren](../../img/tutorial-textstorycreator/converter-tab.png)

Das Ergebnis:
[html](../../documents/pirates-in-the-sea-of-blood/de/html/) - [pdf](../../documents/pirates-in-the-sea-of-blood/de/pdf/abenteuer-in-der-blutigen-see_josa-wode.pdf)

### <a id="auf-zu-neuen-ufern"></a>Und jetzt? Auf zu neuen Ufern!

#### Überarbeiten, überarbeiten, überarbeiten

Die konvertierten Dokumente eignen sich hervoragend zum Korrekturlesen -- hier findest du Fehler, die du in deiner *Textstory*-Datei nicht entdeckt hast. Noch besser klappt das im gedruckten Dokument und wenn du Andere den Text Korrektur lesen lässt.

Auch das Layout kannst du hier nochmal kritisch betrachten und eventuell anpassen. 
Es könnte interessant sein, mit der Schriftgröße zu spielen. Der Text sollte sich möglichst angenehm lesen lassen. Ein grober Richtwert sind Zeilen mit einer Länge von 50-70 Zeichen.

#### Buchdruck-Optionen

Möchtest du deinen Text als Buch drucken (auch für selbstgebastelte Heftchen könnte dies interessant sein), bietet der *Textstory Creator* verschiedene Einstellungen, die dich dabei unterstützen.

*Buchdruck*

Diese Option ermöglicht es u.A., dem Text noch eine [Titelei](https://de.wikipedia.org/wiki/Titelei) voranzustellen oder Anhänge anzufügen.  

Dafür begib dich in dein Ausgabeverzeichnis. Im Unterverzeichnis *latex/bookPreliminaries* kannst du *LaTeX*-Dokumente einfügen, die dem eigentlichen Text vorangestellt werden. Für Anhänge ist das Unterverzeichnis *latex/bookAppendix* vorgesehen.  
Die Dateien werden dem Dokument in alphanumerischer Reihenfolge hinzugefügt.

Vorlagen findest du im Installationsverzeichnis (siehe [Lizenzen im Textstory Creator](#licenses-installdir)) unter *textstory_converter/latex/templates*. Auch hier freue ich mich über Einsendungen (<a href="&#x6d;&#x61;&#x69;&#x6c;&#x74;&#111;&#x3a;&#99;&#x6f;&#100;&#x69;&#x6e;&#103;&#x40;&#102;&#111;&#116;&#111;&#101;&#x6c;&#101;&#x63;&#116;&#x72;&#105;&#99;&#x73;&#x2e;&#x64;&#x65;">Email</a>).

Zur Gestaltung stehen neben dem üblichen *LaTeX*-Befehlssatz die in der Anleitung beschriebenen [Textkommandos](https://bitbucket.org/ausguss/textstory_creator/src/master/README.md#markdown-header-latex-textkommandos) zur Verfügung.

*Format*

Weicht das Format des Buches von den gängigen DIN-Formaten ab, kann die Angabe *Definition der Seitengröße* auf *Breite/Höhe* umgestellt werden. Nun ist eine genaue Angabe der *Seitenbreite* und *Seitenhöhe*, z.B. in Millimetern, möglich.

*Bundversatz*

Bei Büchern verschwindet ein Teil der Seite in der Bindung. Um zu vermeiden, dass der Text nun zu weit nach innen rutscht (und somit schlecht lesbar wird), kann mittels der Option *Bundversatz* angegeben werden, wie groß der durch die Bindung verschlungene Teil ist. Der Text wird entsprechend nach außen verschoben.

*Kapitel Seitenumbruch*

Bei längeren Romanen beginnen neue Kapitel häufig auf einer neuen Seite. Dies kann mit der Option *Kapitel Seitenumbruch* eingestellt werden.

#### Broschürendruck

Eine Anleitung zum Basteln einer Broschüre findest du [hier](../tutorial-broschure-printing/).

#### Veröffentlichung

Du hast nun eine bildschirmlesefreundliche *Html*-Version deiner Geschichte, sowie ein für den Druck geeignetes *Pdf*. Das Internet bietet zahlreiche Möglichkeiten, wie die Veröffentlichung auf deiner eigenen Webseite oder einer anderen Plattform.  

Für den Druck gibt es, neben dem klassischen Weg der Bewerbung bei einem Verlag (oder der Option alles selber zu machen), inzwischen diverse Anbieter und Optionen zur Selbstveröffentlichung deines Werkes.   
Interessierst du dich dafür, solltest du zunächst die verschiedenen Möglichkeiten kritisch prüfen und miteinander vergleichen. Welche Art der Veröffentlichung für dich in Frage kommt, ist eine sehr individuelle Entscheidung, die von vielen Faktoren abhängt. Überlege dir gut, was deine Anforderungen sind. Bei den meisten Angeboten wirst du, anders als bei einem herkömmlichen Verlag, nicht mit nennenswerter Werbung für dein Buch rechnen können. 

Ich habe mich bisher für die *Classic*-Variante bei [Books on Demand](https://www.bod.de/) entschieden. Das Preisleistungsverhältnis erscheint mir dabei sehr gut. Gespart wird in meinen Augen beim Kundenservice, sodass man selber wissen sollte was man tut und nicht mit großer Unterstützung von *BoD* rechnen kann. Zudem ist die Webseite schlecht gemacht und es kann mit gelegentlichen Ausfällen derselben gerechnet werden.

#### Datensicherheit

Schon während des Schaffensprozesses sollte die *Textstory* regelmäßig gesichtert werden. 
Es wäre einfach zu frustrierend, nach stunden-, tage- oder monatelanger Arbeit alles bisher Geschaffene zu verlieren. 
Kein einzelnes Medium ist so sicher, dass diesem vollends vertraut werden kann.

Daher sichere deine Daten regelmäßig zumindest auf verschiedenen physikalisch getrennten Datenträgern (Festplatten, Usb-Sticks, etc.). Auch ein Speichern in der *Cloud* ist sehr verlockend. Hier solltest du dir aber Gedanken zu den Rechten und dem Schutz vor Fremdzugriffen machen. 

Eine interessante Idee könnte sein, Schreibprojekte mit Versionsverwaltungssystemen für Softwareprojekte zu sichern. Dies erfordert zunächst einige Einarbeitung, bietet aber auch gewisse Vorteile. So könntest du jederzeit zu früheren Versionen deines Textes zurückkehren oder auch gemeinsam mit mehreren Personen daran arbeiten. Zudem gibt es Möglichkeiten, das Projekt in verschiedenen sogenannten *Repositories* zu verwalten (z.B. um dein Projekt auf einem Server zu sichern). Es gibt auch Onlineplattformen, die (zum Teil kostenlos) Versionsverwaltung anbieten.  
Am verbreitetsten ist derzeit wohl [Git](https://git-scm.com/). Als Onlineplattformen seien [Bitbucket](https://bitbucket.org/) und [Github](https://github.com/) genannt.
Diese Systeme sind natürlich für das Verwalten von Code vorgesehen und optimiert, aber da es sich bei Code ebenfalls um Text handelt, können sie für Textdokumente ebenfalls brauchbar sein.

### <a id="wasser-unter-dem-kiel"></a>Immer eine Handbreit Wasser unter dem Kiel

Ich hoffe, du kannst mit dem *Textstory Creator* und diesem Tutorial etwas anfangen. Über Anregungen, Kritik, Vorschläge, Lob, etc. freue ich mich. Zögere nicht, mir zu <a href="&#x6d;&#x61;&#x69;&#x6c;&#x74;&#111;&#x3a;&#99;&#x6f;&#100;&#x69;&#x6e;&#103;&#x40;&#102;&#111;&#116;&#111;&#101;&#x6c;&#101;&#x63;&#116;&#x72;&#105;&#99;&#x73;&#x2e;&#x64;&#x65;">schreiben</a>. Ich versuche auch gerne, bei Problemen weiterzuhelfen.

Ahoi und gute Reise!
