+++
date = "2017-11-25T15:34:30+01:00"
description = "Hier erkläre ich, wie man eine Broschüre bastelt."
metadescription = ""
metatype = "Anleitung, Tutorial"
publishlicense = "Creative Commons BY-NC-SA 3.0"
title = "Anleitung zum Broschürendruck"
writers = "Josa Wode"

+++
<a href="/img/broschure-printing/large/14-cover-inside.jpg"><img src="/img/broschure-printing/14-cover-inside.jpg" alt="Broschürendruck: Umschlag innen" class="autoscaled leftfloat" /></a>
Es ist heutzutage keine große Kunst mehr, aus einem Text (Bilder und so sind natürlich ebenfalls möglich) ein nettes kleines Heftchen zu basteln. 

In dieser Anleitung erkläre ich, wie man eine Broschüre im DIN A5-Format herstellt. Dies ist eine praktische, angenehm lesbare Größe und lässt sich mit einem gängigen A4-fähigen Drucker leicht umsetzen. 
Eine Übertragung der Anleitung auf andere Formate dürfte kein Problem sein, sofern die entsprechende Drucktechnik zuhause oder im Kopierladen des Vertrauens vorhanden ist.

#### Zur Erläuterung

Dieser Abschnitt kann übersprungen werden, falls Zeit oder Interesse fehlen.  
 
Die grundlegende Idee beim Broschürendruck ist, auf je einem A4-Blatt vier A5-Seiten des zu druckenden Dokuments anzuordnen -- zwei auf der Vorder- und zwei auf der Rückseite -- und das so lange, bis alle Seiten untergebracht sind. Dann werden die Blätter in der Mitte gefaltet und ineinander gelegt, sodass am Ende eine Broschüre dabei herauskommen soll, bei deren Durchblättern die Seiten in der richtigen Reihenfolge angeordnet sind.

Nun gilt noch zu klären, wie die Seiten auf den Blättern verteilt werden müssen. Würden dies einfach in aufsteigender Reihenfolge erfolgen, entstünde nach Falten und Zusammenstecken ein ganz schönes Durcheinander.  
Zunächst einmal gehen wir davon aus, dass die Seitenzahl durch 4 teilbar ist. Sollte dies nicht der Fall sein, denken wir uns einfach entsprechend viele (bis zu drei) Leerseiten  ans Ende. Nun müssen die Seiten wie folgt auf die Blätter verteilt werden:   
Auf die Vorderseite von Blatt 1 kommt links die letzte und rechts die erste Seite, auf die Rückseite links die zweite und rechts die vorletzte.  
Bei der Vorderseite von Blatt 2 ist es links die vorvorletzte und rechts die dritte Seite, auf der Rückseite geht es links mit Seite 4 weiter und rechts... du kannst es dir denken.   
So wird sich bis zur Broschürenmitte vorgearbeitet.

Ein kleines Beispiel einer Broschüre mit stolzen 8 Seiten:  
Blatt 1 Vorderseite: links Seite 8, rechts Seite 1  
Blatt 1 Rückseite: links Seite 2, rechts Seite 7  
Blatt 2 Vorderseite: links Seite 6, rechts Seite 3  
Blatt 2 Rückseite: links Seite 4, rechts Seite 5  

Zum Glück gibt es Programme, die uns diese Arbeit abnehmen und die Seiten gleich automatisch wie erforderlich anordnen. Bleibt nur noch das Drucken, zu dem ich im nächsten Abschnitt komme, und das Falten der Seiten.
Im Beispiel würde Blatt 1 so gefaltet, dass vorne Seite 1 ist, hinten Seite 8 und innen die Rückseite wie gehabt. Blatt 2 würde entsprechend gefaltet und in Blatt 1 eingelegt. Bei größeren Broschüren geht es mit den übrigen Blättern der Reihe nach so weiter. Fertig.


#### Zum Kern der Sache

<a href="/img/broschure-printing/large/01-acrobat-reader-broschure-printing.png"><img src="/img/broschure-printing/01-acrobat-reader-broschure-printing.png" alt="Broschürendruck: Acrobat Reader" class="autoscaled rightfloat" /></a>
Die meisten aktuellen Textverarbeitungs- und Layoutprogramme dürften eine Option für den Broschürendruck bieten, mit der dieser einfach zu realisieren ist.

Ich gehe in dieser Anleitung davon aus, dass das zu druckende Dokument im pdf-Format vorliegt (andere Dateien lassen sich in der Regel ebenfalls in pdf umwandeln).

Viele verwenden den *Adobe Acrobat Reader* zum Anzeigen und Drucken von pdf-Dateien. 
Dort kann im Druckmenü unter *Seite anpassen und Optionen* die Auswahl *Broschüre* getroffen werden. Unter diesem Punkt gibt es ein paar wenige Einstellmöglichkeiten. 

<a href="/img/broschure-printing/large/02-pdf-xchange-viewer-broschure-printing.png"><img src="/img/broschure-printing/02-pdf-xchange-viewer-broschure-printing.png" alt="Broschürendruck: PDF-XChange Viewer" class="autoscaled leftfloat" /></a>
Ich bevorzuge den, ebenfalls kostenlos verfügbaren, *[PDF-XChange Viewer](https://www.tracker-software.com/product/pdf-xchange-viewer)* und gehe im folgenden von dessen Optionen aus. Bei Verwendung anderer Software müssen meine Angaben entsprechend übertragen werden.

Diejenigen mit einem Drucker, der beidseitig drucken kann, dürfen sich glücklich schätzen und können sich einfach die gesamte Broschüre in einem Rutsch ausspucken lassen. Für alle anderen empfehle ich folgendes Vorgehen: 

Öffnet euer pdf-Dokument und wählt die Option *Print...* im Dateimenü. Dort ist der Abschnitt *Page Scaling* für uns interessant. Als *Scaling Type* ist *Booklet* zu wählen, als *Type* *Broschure*. Zunächst druckt die Vorderseiten, indem ihr bei *Side(s)* *Front* auswählt.

<a href="/img/broschure-printing/large/03-printing.jpg"><img alt="Broschürendruck: Einlegen der Blätter in den Drucker" src="/img/broschure-printing/03-printing.jpg" class="autoscaled rightfloat" /></a>
Sind die Vorderseiten bedruckt, legt diese erneut in den Drucker ein. 
Die Überlegung, wie genau die Blätter einzulegen sind, kann Knoten im Kopf verursachen. Dies ist von Drucker zu Drucker unterschiedlich und es finden sich eventuell mehr oder weniger hilfreiche Hinweise in Bedienungsanleitung, auf dem Drucker oder sonstwo. Da mein Kopf Blätter anders zu drehen und wenden scheint als mein Drucker, habe ich leider einige Fehlversuche gehabt, bis ich mir schließlich eigene Hinweiszettel auf den Drucker geklebt habe, sodass in Zukunft weniger schief gehen kann. 

Nun können die Rückseiten gedruckt werden, indem *Side(s)* auf *Back* eingestellt wird.

Wenn ihr noch nicht genau wisst, was ihr tut beziehungsweise was euer Drucker tut, empfehle ich, es erstmal mit einer Seite auszuprobieren, bevor ihr einen Haufen Altpapier produziert. Im *PDF-XChange Viewer* findet sich die Option dazu im Druckmenü unten rechts unter der Druckvorschau. Dort tragt ihr bei *Print Sheets* die *1* in das Feld ein. Sollte alles geklappt haben, tragt ihr beim nächsten Drucken natürlich dort *2-x* ein, wobei *x* euer letztes zu druckendes Blatt ist.

<a href="/img/broschure-printing/large/04-folding.jpg"><img src="/img/broschure-printing/04-folding.jpg" alt="Broschürendruck: Falten der Blätter" class="autoscaled leftfloat" /></a>
Hat etwas nicht geklappt, entnehmt erstmal die einseitig bedruckten Blätter, druckt dann auf ein frisches erneut die erste Vorderseite (da das erste Blatt ja nun versaut ist), legt sie auf euren einseitig bedruckten Stapel passend auf und diesen dann wieder in den Drucker ein -- diesmal richtig herum (wie es nicht geht, habt ihr ja gerade herausgefunden).

Habt ihr alles gedruckt, seid ihr im Grunde fertig. Die Blätter müssen nun nur noch gefaltet und ineinander gelegt werden. Beim ersten Blatt kommen die erste und letzte Seite nach außen, die zweite und die vorletzte nach innen. Nach diesem Prinzip fahrt mit den anderen Blättern fort. 
Faltet die Blätter am besten einzeln und geht dabei sorgfältig vor, damit das Resultat nicht schief und zerfleddert aussieht. Jedes gefaltete Blatt wird in die vorhergehenden gelegt.

#### Binden

<a href="/img/broschure-printing/large/06-sewing-accessories.jpg"><img src="/img/broschure-printing/06-sewing-accessories.jpg" alt="Broschürendruck: Nähzubehör" class="autoscaled rightfloat" /></a>
Eine schöne Möglichkeit, die Seiten zusammen zu halten und dem Ganzen etwas mehr den Anschein eines Büchleins zu geben, ist, sie zusammen zu nähen. 

Dazu wird eine Nadel, etwas Faden und ein Fingerhut benötigt. Letzterer ist dazu da, genügend Kraft auf die Nadel bringen zu können, um die Blätter zu durchstechen, ohne sich dabei die Finger kaputt zu machen.

<a href="/img/broschure-printing/large/08-sewing-outside.jpg"><img src="/img/broschure-printing/08-sewing-outside.jpg" alt="Broschürendruck: Nähen (außen)" class="autoscaled leftfloat" /></a>
Die nötige Fadenlänge ist abzuschätzen. Nehmt ihr zu wenig, müsst ihr zwischendurch mit einem weiteren Faden weitermachen und das Resultat wird etwas unsauberer, nehmt ihr zu viel, müsst ihr bei jedem Stich viel Faden durchziehen, verheddert euch leicht und verschwendet am Ende das übrig gebliebene Fadenstück. Mit etwa einem Meter Faden bin ich gut hingekommen.

<a href="/img/broschure-printing/large/09-sewing-inside-progressed.jpg"><img src="/img/broschure-printing/09-sewing-inside-progressed.jpg" alt="Broschürendruck: Nähen (innen) fortgeschritten" class="autoscaled rightfloat" /></a>
Ich entscheide mich, im Inneren der Broschüre zu beginnen und nähe mit etwas Abstand vom Rand von unten nach oben. Es kann auch außen und/oder oben begonnen werden.

Beim Durchstechen der Blätter sollte möglichst genau der Knick in der Mitte aller Doppelseiten getroffen werden. Das ist nicht ganz einfach, da für das Durchstechen aller Blätter etwas Kraft nötig ist und die Blätter leicht verrutschen. Sie sollten möglichst gerade aufeinander liegen, auch damit nicht am Ende Blätter an einer Seite aus dem Heft herausragen. 
<a href="/img/broschure-printing/large/10-sewing-inside-done.jpg"><img src="/img/broschure-printing/10-sewing-inside-done.jpg" alt="Broschürendruck: Naht (innen)" class="autoscaled leftfloat" /></a>
Leichte Abweichungen sind kein Drama und beeinträchtigen nur die Optik ein bisschen.

Es gibt eine Vielzahl verschiedener Nähstiche und so ziemlich jeder, der in gerader Linie verläuft und halbwegs stabil ist, dürfte geeignet sein. 
Ich nähe mit einer Art Rückstich, bei dem ich immer wieder zu einem vorigen Loch zurückkehre. 
Dadurch wird die Naht stabil und es entsteht eine halbwegs gerade durchgehende Linie.   

Beim ersten Stich lasse ich etwas Faden überstehen, damit sich die Naht nicht von selbst wieder löst. 
Das lose Ende lege ich nach oben in den Seitenknick und nähe es bei meinen ersten Stichen mit ein. 
<a href="/img/broschure-printing/large/12-binding-complete.jpg"><img src="/img/broschure-printing/12-binding-complete.jpg" alt="Broschürendruck: Fertig gebundene Broschüre" class="autoscaled rightfloat" /></a>
Auch am anderen Ende möchte ich sicherstellen, dass sich meine Naht nicht öffnet, daher mache ich einige Stiche rückwärts, bevor ich den Restfaden kurz abschneide.

Diesmal habe ich leider etwas unsauber gearbeitet und etwa auf der Hälfte den Faden durchgerissen, sodass ich dort erneut ansetzen musste.
So hatte ich weitere Fadenenden zu vernähen, die das Gesamtbild etwas stören. Dennoch finde ich, dass sich das Ergebnis sehen lassen kann. 

Es lohnt sich also, sorgfältig zu arbeiten, kleinere Unsauberheiten sind dennoch verkraftbar.

#### Umschlag

<a href="/img/broschure-printing/large/13-cover.jpg"><img src="/img/broschure-printing/13-cover.jpg" alt="Broschürendruck: Umschlag" class="autoscaled leftfloat" /></a>
Nun fehlt eigentlich nur noch ein schicker Umschlag zum Glück. 

Dafür bietet sich Bastelkarton in gewünschter Farbe an. Den Karton habe ich größer als A4 zugeschnitten, sodass er an den Rändern etwas über die Broschüre hinaus reicht. Es wäre natürlich auch eine Option, den Karton exakt auf Broschürengröße zuzuschneiden (oder gleich A4-Karton zu verwenden), nur finde ich die Sache mit Überstand schöner. 

Das zugeschnittene Stück wird mittig gefaltet und nach Belieben gestaltet. Ich nehme gerne schwarzen Karton und beschrifte ihn mit einem weißem Stift.

<a href="/img/broschure-printing/large/15-broschure-glued.jpg"><img src="/img/broschure-printing/15-broschure-glued.jpg" alt="Broschürendruck: Broschüre eingeklebt" class="autoscaled rightfloat" /></a>
Bisher habe ich die Broschüre einfach im Umschlag fixiert, indem ich sie mit der -- bei mir freien -- Rückseite mit Klebestift eingeklebt habe. 

Beim nächsten Mal möchte ich sie einnähen, da ich denke, dass das noch besser aussieht. Um den Nähvorgang für die Broschüre nicht zusätzlich zu erschweren, würde ich dazu diese zunächst wie gehabt binden und dann mit einem weiteren Faden durch die bereits vorhandenen Löcher den Umschlag annähen.
