+++
date = "2018-10-06"
writers = "Josa Wode"
description = "Ein Tag auf dem Friedhof"
publishlicense = "Creative Commons BY-NC-SA 3.0"
metatype = "Eine kurze Geschichte"
metadescription = ""
title = "Oktober"

nosupportinfo = "no"
+++
Dies ist eine kurze Oktobergeschichte. Sie ist Teil des als Taschenbuch erhältlichen Sammelbandes [Leben, Träumen und Sterben](../live-dream-die/). Bis zum Jahresende wird zudem jeden Monat eine weitere Geschichte aus der Reihe auf meiner Seite erscheinen.

#### Zum Inhalt

*Fantasie, Düsterromantik*  
Lira genießt die Ruhe auf einem der zahlreichen Friedhöfe Prags und macht dabei eine außergewöhnliche Begegnung.

#### Lesen
[html](../../documents/october/de/html/) - [pdf](../../documents/october/de/pdf/oktober_josa-wode.pdf)

**Druckempfehlung**: Das Format des Pdf-Dokuments ist DIN A5, daher sollten zwei Seiten auf eine (A4-)Seite gedruckt werden (und natürlich beidseitig, um Papier zu sparen).  
Wie man eine Broschüre druckt (und bastelt) erkläre ich in [dieser Anleitung](../tutorial-broschure-printing/).

#### Änderungen
07.01.2018 -- Korrigierte Erstfassung



