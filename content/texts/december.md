+++
date = "2021-12-01"
writers = "Josa Wode"
description = "Neuanfang"
publishlicense = "Creative Commons BY-NC-SA 3.0"
metatype = "Eine kurze Geschichte"
metadescription = ""
title = "Dezember"

nosupportinfo = "no"
+++
Diese kurze Dezembergeschichte ist Teil des als Taschenbuch erhältlichen Sammelbandes [Leben, Träumen und Sterben](../live-dream-die/).

#### Zum Inhalt

*Rassismus, Solidarität*  
Eine neue Stadt in einem fremden Land. Begegnungen.

#### Lesen
[html](../../documents/december/de/html/) - [pdf](../../documents/december/de/pdf/dezember_josa-wode.pdf)

**Druckempfehlung**: Das Format des Pdf-Dokuments ist DIN A5, daher sollten zwei Seiten auf eine (A4-)Seite gedruckt werden (und natürlich beidseitig, um Papier zu sparen).  
Wie man eine Broschüre druckt (und bastelt) erkläre ich in [dieser Anleitung](../tutorial-broschure-printing/).

#### Änderungen
07.01.2018 -- Korrigierte Erstfassung



