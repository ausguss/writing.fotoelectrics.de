+++
date = "2018-04-01"
draft = false
description = "Ein Tag für Späße"
metadescription = ""
metatype = "Eine kurze Geschichte"
nosupportinfo = "no"
publishlicense = "Creative Commons BY-NC-SA 3.0"
title = "April"
writers = "Josa Wode"

+++
Dies ist eine kurze Aprilgeschichte. Sie ist Teil des als Taschenbuch erhältlichen Sammelbandes [Leben, Träumen und Sterben](../live-dream-die/). Bis zum Jahresende wird zudem jeden Monat eine weitere Geschichte aus der Reihe auf meiner Seite erscheinen.

#### Zum Inhalt

*Fantasie, Macht des Zufalls*  
Ausgestoßen und verbannt nutzt *Es* dennoch *seinen* Einfluss und treibt *sein* Spiel.

#### Lesen
[html](../../documents/april/de/html/) - [pdf](../../documents/april/de/pdf/april_josa-wode.pdf)

**Druckempfehlung**: Das Format des Pdf-Dokuments ist DIN A5, daher sollten zwei Seiten auf eine (A4-)Seite gedruckt werden (und natürlich beidseitig, um Papier zu sparen).  
[Anleitung zum Broschürendruck](../tutorial-broschure-printing/)

#### Änderungen
06.02.2018 -- Korrigierte Erstfassung


