+++
date = "2016-11-02"
draft = false
title = "Kommt und holt sie"
description = "Es könnte ein Märchen sein, wenn es sich benehmen würde."
metadescription = ""
metatype = "Eine kurze Geschichte"
publishlicense = "Creative Commons BY-NC-SA 3.0"
writers = "Josa Wode"
+++
**Kommt und holt sie** ist eine kurze Geschichte, deren Erstfassung ich Ende Juli 2015 fertiggestellt habe und die inzwischen einige Überarbeitung erfahren hat.

#### Zum Inhalt

Das Verschwinden einer Prinzessin versetzt einen ganzen Königshof in Aufruhr. In einem großen Ritterturnier werden die fähigsten Recken des ganzen Landes auserkoren, die verschollene Königstochter Karina aus den Fängen einer bösartigen Hexe zu befreien und ihrem rechtmäßigen Besitzer ...äh... liebenden Vater zurückzubringen.  
Der Weg birgt viele Gefahren und Hindernisse für die wagemutigen Abenteurer, die ihre Stärken zu nutzen wissen und mit ihren Schwächen zu kämpfen haben. Wird einer von ihnen obsiegen und somit die Hand der Prinzessin als Preis gewinnen? Die Hand soll dabei natürlich fest mit der Prinzessin verbunden bleiben -- zur Heirat.  
Doch was hält die eigensinnige Karina von der ganzen Angelegenheit? Wie ergeht es ihr am Königshof und was erlebt sie bei ihrem Verschwinden?
Statt des passiven Subjekts der zu rettenden Prinzessin steht im Mittelpunkt der Geschichte ein aufgewecktes selbständiges Kind, das viele Abenteuer erlebt, Freundinnen und Freunde gewinnt und sich von Fremdbestimmung und Bevormundung zu emanzipieren sucht.
Dabei bricht die Erzählung mit manchem Märchenklischee.

#### Lesen
[html](../../documents/come-get-her/de/html/) - [pdf](../../documents/come-get-her/de/pdf/kommt-und-holt-sie_josa-wode.pdf)  

**Druckempfehlung**: Das Format des Pdf-Dokuments ist DIN A5, daher sollten zwei Seiten auf eine (A4-)Seite gedruckt werden (und natürlich beidseitig, um Papier zu sparen).  
[Anleitung zum Broschürendruck](../tutorial-broschure-printing/)

#### Änderungen
24.10.2016 -- Einleitenden Abschnitt ergänzt, kleinere Korrekturen  
14.03.2016 -- Einige kleinere Korrekturen  
17.10.2015 -- Inhaltliche Überarbeitung  
25.09.2015 -- Neues Layout und HTML-Version  
29.07.2015 -- Zahlreiche Korrekturen  
21.07.2015 -- Erstfassung (ohne Korrektur) 

Vielen Dank für all die Anmerkungen und Korrekturvorschläge. 
