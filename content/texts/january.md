+++
date = "2018-01-01"
draft = false
title = "Januar"
description = "Ein verkaterter Neujahrstag läd zum Träumen ein"
metadescription = ""
metatype = "Eine kurze Geschichte"
publishlicense = "Creative Commons BY-NC-SA 3.0"
writers = "Josa Wode"
+++
Dies ist eine kurze Januargeschichte. Sie ist Teil des als Taschenbuch erhältlichen Sammelbandes [Leben, Träumen und Sterben](../live-dream-die/). Dieses Jahr wird zudem jeden Monat eine weitere Geschichte aus der Reihe auf meiner Seite erscheinen.

#### Zum Inhalt

*Traum, Surreales*   
Den ersten Tag im Jahr gilt es (für Viele), klar zu kommen und sich von der vorangegangenen Nacht zu erholen. Genau das tut die Protagonistin in dieser Geschichte. 

#### Lesen
[html](../../documents/january/de/html/) - [pdf](../../documents/january/de/pdf/januar_josa-wode.pdf)  

**Druckempfehlung**: Das Format des Pdf-Dokuments ist DIN A5, daher sollten zwei Seiten auf eine (A4-)Seite gedruckt werden (und natürlich beidseitig, um Papier zu sparen).  
[Anleitung zum Broschürendruck](../tutorial-broschure-printing/)

#### Änderungen
21.12.2017 -- Korrigierte Erstfassung 

