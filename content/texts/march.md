+++
date = "2018-03-01"
draft = false
description = "Ermittlungen nehmen einen unerwarteten Lauf"
metadescription = ""
metatype = "Eine kurze Geschichte"
nosupportinfo = "no"
publishlicense = "Creative Commons BY-NC-SA 3.0"
title = "März"
writers = "Josa Wode"

+++
Dies ist eine kurze Märzgeschichte. Sie ist Teil des als Taschenbuch erhältlichen Sammelbandes [Leben, Träumen und Sterben](../live-dream-die/). Bis zum Jahresende wird zudem jeden Monat eine weitere Geschichte aus der Reihe auf meiner Seite erscheinen.

#### Zum Inhalt

*Horror, Ausweglosigkeit*  
Eine kalte Spur führt in die Abgeschiedenheit der Berge und von dort zu einem gänzlich anderen Ort. Die Lage gerät außer Kontrolle.

#### Lesen
[html](../../documents/march/de/html/) - [pdf](../../documents/march/de/pdf/märz_josa-wode.pdf)

**Druckempfehlung**: Das Format des Pdf-Dokuments ist DIN A5, daher sollten zwei Seiten auf eine (A4-)Seite gedruckt werden (und natürlich beidseitig, um Papier zu sparen).  
[Anleitung zum Broschürendruck](../tutorial-broschure-printing/)

#### Änderungen
06.02.2018 -- Korrigierte Erstfassung


