+++
date = "2018-02-01"
draft = false
title = "Februar"
description = "Eine Skitour wird abenteuerlicher als geplant"
metadescription = ""
metatype = "Eine kurze Geschichte"
publishlicense = "Creative Commons BY-NC-SA 3.0"
writers = "Josa Wode"
+++
Dies ist eine kurze Februargeschichte. Sie ist Teil des als Taschenbuch erhältlichen Sammelbandes [Leben, Träumen und Sterben](../live-dream-die/). Bis zum Jahresende wird zudem jeden Monat eine weitere Geschichte aus der Reihe auf meiner Seite erscheinen.

#### Zum Inhalt

*Spannung, Abenteuer*   
Der Natur begegnen, in unberührte Schneelandschaften vordringen, den eigenen Körper spüren. 
Dieses Erlebnis wird für David und Laura zu etwas Unvergesslichem -- doch nicht so, wie sie es sich vorgestellt haben.

#### Lesen
[html](../../documents/february/de/html/) - [pdf](../../documents/february/de/pdf/februar_josa-wode.pdf)  

**Druckempfehlung**: Das Format des Pdf-Dokuments ist DIN A5, daher sollten zwei Seiten auf eine (A4-)Seite gedruckt werden (und natürlich beidseitig, um Papier zu sparen).  
[Anleitung zum Broschürendruck](../tutorial-broschure-printing/)

#### Änderungen
06.02.2018 -- Kleine Korrektur  
07.01.2018 -- Korrigierte Erstfassung 

