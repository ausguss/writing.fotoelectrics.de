+++
date = "2018-07-01"
writers = "Josa Wode"
description = "Geocaching mal anders"
publishlicense = "Creative Commons BY-NC-SA 3.0"
metatype = "Eine kurze Geschichte"
metadescription = ""
title = "Juli"

nosupportinfo = "no"
+++
Dies ist eine kurze Juligeschichte. Sie ist Teil des als Taschenbuch erhältlichen Sammelbandes [Leben, Träumen und Sterben](../live-dream-die/). Bis zum Jahresende wird zudem jeden Monat eine weitere Geschichte aus der Reihe auf meiner Seite erscheinen.

#### Zum Inhalt

*Grenzerfahrung, Abenteuer*  
Eine moderne Schatzsuche wird zu bitterem Ernst.

#### Lesen
[html](../../documents/july/de/html/) - [pdf](../../documents/july/de/pdf/juli_josa-wode.pdf)

**Druckempfehlung**: Das Format des Pdf-Dokuments ist DIN A5, daher sollten zwei Seiten auf eine (A4-)Seite gedruckt werden (und natürlich beidseitig, um Papier zu sparen).  
Wie man eine Broschüre druckt (und bastelt) erkläre ich in [dieser Anleitung](../tutorial-broschure-printing/).

#### Änderungen
06.01.2018 -- Korrigierte Erstfassung



