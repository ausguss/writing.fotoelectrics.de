+++
date = "2018-06-01"
writers = "Josa Wode"
description = "Mittsommer"
publishlicense = "Creative Commons BY-NC-SA 3.0"
metatype = "Eine kurze Geschichte"
metadescription = ""
title = "Juni"

nosupportinfo = "no"
+++
Dies ist eine kurze Junigeschichte. Sie ist Teil des als Taschenbuch erhältlichen Sammelbandes [Leben, Träumen und Sterben](../live-dream-die/). Bis zum Jahresende wird zudem jeden Monat eine weitere Geschichte aus der Reihe auf meiner Seite erscheinen.

#### Zum Inhalt

*Tragik, Widerstand*  
Das langersehnte Mittsommerfest in der imperialen Provinz wird zu etwas ganz Besonderem.

#### Lesen
[html](../../documents/june/de/html/) - [pdf](../../documents/june/de/pdf/juni_josa-wode.pdf)

**Druckempfehlung**: Das Format des Pdf-Dokuments ist DIN A5, daher sollten zwei Seiten auf eine (A4-)Seite gedruckt werden (und natürlich beidseitig, um Papier zu sparen).  
[Anleitung zum Broschürendruck](../tutorial-broschure-printing/)

#### Änderungen
31.05.2018 -- Kommafehler ;)  
27.01.2018 -- Korrigierte Erstfassung



