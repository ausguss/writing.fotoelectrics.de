<!DOCTYPE HTML>  
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />    

  <link href="css/styles.css" rel="stylesheet" />
</head>
<body>  

<?php
//time behaviour
$minAllowedTime = 2;//minimum time that has to be spent on the site before sending data (in seconds)

$loadTime = microtime(TRUE);
if (isset($_POST["loadTime"])) {
  $startTime = $_POST["loadTime"];
}

// define variables and set to empty values
$nameErr = $emailErr = $roboErr = "";
$name = $email = $comment = "";
$userInfo = "";
$website = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  //check if someone likes the honey too much
  if (!empty($_POST["website"])) {
    $roboErr = "bad robot";
  }
  //check if someone is a bit too eager
  $sentTime = microtime(TRUE);
  $timeDiff = $sentTime - $startTime;
  $timeDiff = $timeDiff;
  if ($timeDiff < $minAllowedTime) {
    $roboErr = "bad fast robot";
    $userInfo = "Nicht so schnell!";//"send: " . $timeDiff . ", min allowed:  " . $minAllowedTime;
  }

  $nameErrColor = "#000000";
  if (empty($_POST["name"])) {
    $nameErrColor = "#FF0000";
    $nameErr = "Bitte Name angeben";
  } else {
    $name = test_input($_POST["name"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z \.]*$/",$name)) {
      $nameErrColor = "#FF0000";
      $nameErr = "Nur Buchstaben und Leerzeichen sind erlaubt";
    }
  }
  
  $mailErrColor = "#000000";
  if (empty($_POST["email"])) {
    $mailErrColor = "#FF0000";
    $emailErr = "Bitte Emailadresse angeben";
  } else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $mailErrColor = "#FF0000";
      $emailErr = "Ungültige Emailadresse";
    }
  }
    
  if (empty($_POST["comment"])) {
    $comment = "";
  } else {
    $comment = test_input($_POST["comment"]);
  }

  if (!$nameErr and !$emailErr and !$roboErr) {
    //send mail

    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= "From: writing@fotoelectrics.de" . "\r\n";
    
    $msg = "Hallo Josa,<br/><br/>ich möchte gerne über neue Veröffentlichungen von dir auf dem Laufenden bleiben. Bitte nimm mich mit folgender Emailadresse in den Verteiler auf:<br/><br/>";
    $msg .= $email . "<br/><br/>";
    if ($comment) {
      $msg .= "Persönliche Nachricht:<br/>";
      $msg .= nl2br($comment) . "<br/><br/>";
    }
    $msg .= "Viele Grüße<br/>" . $name;

    $mailReturn = mail("writing@fotoelectrics.de", "Newsletter abonnieren", $msg, $headers);
    if ($mailReturn == FALSE) {
      $userInfo = "Ups. Leider konnte deine Anfrage nicht gesendet werden. Probiere es am besten (später) nochmal";
    } else {
      $userInfo = "Vielen Dank für dein Interesse. Ich halte dich von nun an auf dem Laufenden.";
    }
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

<h3>Newsletter bestellen</h3>
<form class="newsletter" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="text/plain; charset=utf-8">  
  <p><strong>Name:</strong> </br><input type="text" name="name" value="<?php echo $name;?>"/>
  <span style="color:<?php print $mailErrColor; ?>;">* <?php echo $nameErr;?></span></p>
  <p><strong>Email:</strong> </br><input type="text" name="email" value="<?php echo $email;?>"/>
  <span style="color:<?php print $mailErrColor; ?>;" >* <?php echo $emailErr;?></span></p>
  <p><strong>Persönliche Nachricht:</strong> </br><textarea name="comment" rows="5" cols="40" style="resize:none; max-width:100%;" ><?php echo $comment;?></textarea></p>
  <p class="text-muted"> * erforderlich<input type="text" class="newsletter-website-field" name="website" placeholder="bitte nicht ausfüllen" value="<?php echo $website;?>"/><input type="hidden" name="loadTime" value="<?php echo "$loadTime";?>"/></p>
  <input type="submit" name="submit" value="Absenden">
</form>

<?php
  if ($userInfo) {
    echo "<br/>";
    echo "<p><strong>" . $userInfo . "</strong></p>";
  }
?>

</body>
</html>

